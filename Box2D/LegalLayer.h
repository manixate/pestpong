//
//  Legal.h
//  Box2D
//
//  Created by Azeem on 03/12/2012.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@protocol ModalLayerDelegate, PopupDelegate;

@interface LegalLayer : CCNode <UITextFieldDelegate, ModalLayerDelegate>
{
    CCLabelBMFont *title;

    UIButton *checkBox, *submitButton;
    
    CCTexture2D *uncheckedTex, *checkedTex;
    bool isChecked;
}

@property (nonatomic) bool blockClose;
@property (nonatomic, assign) id<PopupDelegate> popupDelegate;
@property (nonatomic, retain) UITextField *nameTextView;
@property (nonatomic, retain) UITextField *emailTextView;
@property (nonatomic, retain) NSString *name, *email;

- (BOOL)shallClose;

@end