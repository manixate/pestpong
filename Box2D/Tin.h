//
//  Tin.h
//  Box2D
//
//  Created by Azeem on 29/11/2012.
//
//

#import "CCSprite.h"
#import "b2Fixture.h"

@interface Tin : CCSprite

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSArray *allowedPestType;
@property (nonatomic, retain) NSString *description;
@property (nonatomic) int score;
@property (nonatomic) int multiplier;
@property (nonatomic, assign) CCSprite *frontSprite;
@property (nonatomic, assign) CCSprite *tinLogo;
@property (nonatomic) b2Fixture *bottomFixture;

- (id)initWithFile:(NSString *)filename name:(NSString *)aName description:(NSString *)aDescription allowedType:(NSArray *)aAllowed frontSprite:(CCSprite *)aFrontSprite andTinLogo:(CCSprite *)aTinLogo;

- (bool)isAllowed:(NSString *)pestType;
- (void)setTinPosition:(CGPoint)position;

@end