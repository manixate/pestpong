//
//  SelectionLayer.m
//  Box2D
//
//  Created by Azeem on 07/12/2012.
//
//

#import "SelectionLayer.h"
#import "ModalPopup.h"
#import "GameManager.h"
#import "SimpleAudioEngine.h"

@implementation SelectionLayer
@synthesize popupDelegate;

- (id)init
{
    self = [super init];
    if (self)
    {
        // Add Popup Background rotated
        CCSprite *backgroundSprite = [CCSprite spriteWithFile:@"BasicPopupRotated.png"];
        [self addChild:backgroundSprite];
        
        self.isTouchEnabled = YES;
        self.contentSize = backgroundSprite.contentSize;
        
        CCMenuItemLabel *scenario1 = [CCMenuItemLabel itemWithLabel:[CCLabelTTF labelWithString:@"Scenario 1" fontName:@"Soup Of Justice" fontSize:36] block:^(id sender) {
            GameManager *gameManager = [GameManager gameManager];
            gameManager.currentScenario = 0;
            
            [popupDelegate closePopup];
        }];
        scenario1.color = ccc3(235, 111, 63);
        
        CCMenuItemLabel *scenario2 = [CCMenuItemLabel itemWithLabel:[CCLabelTTF labelWithString:@"Scenario 2" fontName:@"Soup Of Justice" fontSize:36] block:^(id sender) {
            GameManager *gameManager = [GameManager gameManager];
            gameManager.currentScenario = 1;
            
            [popupDelegate closePopup];
        }];
        scenario2.color = ccc3(235, 111, 63);
        
        CCMenuItemLabel *scenario3 = [CCMenuItemLabel itemWithLabel:[CCLabelTTF labelWithString:@"Scenario 3" fontName:@"Soup Of Justice" fontSize:36] block:^(id sender) {
            GameManager *gameManager = [GameManager gameManager];
            gameManager.currentScenario = 2;
            
            [popupDelegate closePopup];
        }];
        scenario3.color = ccc3(235, 111, 63);
        
        CCLayer *menuLayer = [[[CCLayer alloc] init] autorelease];
        [self addChild:menuLayer];
        
        // Add Menu
        CCMenu *menu = [CCMenu menuWithItems:scenario1, scenario2, scenario3, nil];
        [menu alignItemsHorizontallyWithPadding:50];
        menu.position = ccp(0, 0);
        [menuLayer addChild:menu];
    }
    return self;
}

- (void)onExit
{
    [[[CCDirector sharedDirector] touchDispatcher] removeDelegate:self];
    
    // Play Sound
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    // Play Sound
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"GameShowFever.mp3" loop:NO];
    
    [super onExit];
}

- (void)registerWithTouchDispatcher
{
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:-1 swallowsTouches:NO];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return true;
}

#pragma mark - ModalLayerDelegate Methods
- (BOOL)shallClose
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Scenario Selection" message:@"Please select a scenario" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
    
    return false;
}

@end
