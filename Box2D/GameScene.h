//
//  GameScene.h
//  Box2D
//
//  Created by Azeem on 17/11/2012.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "Pest.h"
#import "Tin.h"
#import "B2DebugDrawLayer.h"
#import "MyContactListener.h"
#import "ModalPopup.h"
#import "GameManager.h"

#define PTM_RATIO 32
#define kGameLayer 1000

#pragma mark - ZINDEX
#define kBackground         -1
#define kMovingClouds       1
#define kBehindPlatform     2
#define kPlatform           3
#define kTins               4
#define kInTin              5
#define kFrontTin           6
#define kGameElements       7
#define kHudLayer           9
#define kTransition         10
#define kDebugLayer         9999

#pragma mark - TAG
#define kBackgroundSprite   1
#define kCloud1             2
#define kCloud2             3
#define kPlatformSprite     4
#define kMovingHand         5

#define kHUD                6
#define kNextBar            7
#define kProgressBar        8
#define kScoreLabel         9

#define kLeftGrass          10
#define kRightGrass         11
#define kGrassBackground    12

#define kPest               13

#define kMaxYPower          50

#define ccpToMeter(__point__) ccp(__point__.x/PTM_RATIO, __point__.y/PTM_RATIO)

static uint32_t xor128(void) {
    static uint32_t x = 123456789;
    static uint32_t y = 362436069;
    static uint32_t z = 521288629;
    static uint32_t w = 88675123;
    uint32_t t;
    
    t = x ^ (x << 11);
    x = y; y = z; z = w;
    return w = w ^ (w >> 19) ^ (t ^ (t >> 8));
}

enum CatergoryBit {
    BOUNDARY =          0x0001,
    BASKET_WALLS =      0x0002,
    FLOOR =             0x0004,
    BALL =              0x0008,
};

@interface GameScene : CCLayer <PauseLayerDelegate, UIGestureRecognizerDelegate>
{
    NSDictionary *gameData;
    
    CCTexture2D *spriteTexture_;	// weak ref
    b2World *world;
    B2DebugDrawLayer *debugDraw;
    MyContactListener *contactListener;
    
    CGSize screenSize;
    
    CCLayer *hudLayer;
    
    NSMutableArray *pestList;
    NSMutableArray *selectedList;
    
    NSArray *pests;
    
    BOOL isMoving;
    
    Pest *selectedPest;
    
    NSMutableArray *movingPests;
    
    NSArray *tins;
    
    int score;
    
    float time;
    
    NSString *gameType;
    float pestTimeout;
    
    bool isTimeOver;
    bool isPaused;
    
    CGPoint firstTouch;
    CGPoint lastTouch;
    float firstTime;
    float lastTime;
    
    int grassSound, brushSound, birdSound;
}

+ (CCScene *)scene;
- (void)pause;
- (void)resume;
- (void)drawPlusPoint:(CCSprite *)tin;
- (void)drawMinusPoint:(CCSprite *)tin;
- (void)drawMissPoint:(CGPoint)pos;
- (void)splashTin:(Tin *)tin;
- (void)timeUp;

- (void)nextLevel;
- (void)replayLevel;

- (void)randomizeTins;
@end
