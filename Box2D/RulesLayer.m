//
//  RulesLayer.m
//  Box2D
//
//  Created by Azeem on 05/12/2012.
//
//

#import "RulesLayer.h"
#import "ModalPopup.h"
#import "GameManager.h"

@implementation RulesLayer
@synthesize popupDelegate;

- (id)init
{
    self = [super init];
    if (self)
    {
        // Get the corresponding how to play screen
        int scenario = [[GameManager gameManager] currentScenario];
        
        NSString *howToPlayFileName = [NSString stringWithFormat:@"Scenario%iHowToPlay.png", scenario + 1];
        
        CCSprite *sprite = [CCSprite spriteWithFile:howToPlayFileName];
        [self addChild:sprite];
        
        self.contentSize = sprite.contentSize;
        
        // Add Close Button
        CCSprite *closeSprite = [CCSprite spriteWithFile:@"closeButtonSprite.png"];
        closeSprite.position = ccp(self.contentSize.width / 2 - 48, self.contentSize.height / 2 - 48);
        [self addChild:closeSprite];
        
        closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.selected = NO;
        closeButton.frame = CGRectMake(946, 38, closeSprite.contentSize.width, closeSprite.contentSize.height);
        [closeButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchDown];
        closeButton.backgroundColor = [UIColor clearColor];
        
        [[[CCDirector sharedDirector] view] addSubview:closeButton];
    }
    return self;
}

- (void)onExit
{
    [closeButton removeFromSuperview];
}

- (void)buttonTapped:(UIButton *)sender
{
    sender.enabled = false;
    [popupDelegate closePopup];
}

#pragma mark - ModalLayerDelegate
- (BOOL)shallClose
{
    return true;
}

@end
