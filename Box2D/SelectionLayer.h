//
//  SelectionLayer.h
//  Box2D
//
//  Created by Azeem on 07/12/2012.
//
//

#import "cocos2d.h"

@protocol ModalLayerDelegate, PopupDelegate;

@interface SelectionLayer : CCLayer<ModalLayerDelegate, CCTargetedTouchDelegate>
{
    UIButton *button1, *button2, *button3;
}

@property (nonatomic, assign) id<PopupDelegate>popupDelegate;

@end
