//
//  Leaderboard.h
//  Box2D
//
//  Created by Azeem on 05/12/2012.
//
//

#import "cocos2d.h"

@protocol ModalLayerDelegate, PopupDelegate;

@interface Leaderboard : CCNode<ModalLayerDelegate>
{
    CCLabelBMFont *title;
    CCLabelBMFont *number;
    CCLabelBMFont *name;
    CCLabelBMFont *score;
}

@property (nonatomic, assign) id<PopupDelegate> popupDelegate;

@end
