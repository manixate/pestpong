//
//  Tin.m
//  Box2D
//
//  Created by Azeem on 29/11/2012.
//
//

#import "Tin.h"

@implementation Tin
@synthesize name, score, multiplier, allowedPestType, description, bottomFixture, frontSprite, tinLogo;

- (id)initWithFile:(NSString *)filename name:(NSString *)aName description:(NSString *)aDescription allowedType:(NSArray *)aAllowed frontSprite:(CCSprite *)aFrontSprite andTinLogo:(CCSprite *)aTinLogo
{
    if (self = [super initWithFile:filename])
    {
        name = [aName retain];
        description = [aDescription retain];
        allowedPestType = [aAllowed retain];
        score = 0;
        multiplier = 0;
        frontSprite = aFrontSprite;
        tinLogo = aTinLogo;
    }
    
    return self;
}

- (void)dealloc
{
    bottomFixture = NULL;
    frontSprite = nil;
    tinLogo = nil;
    [name release];
    [allowedPestType release];
    
    [super dealloc];
}

- (bool)isAllowed:(NSString *)pestType
{
    for (NSString *allowed in allowedPestType)
    {
        if ([pestType isEqualToString:allowed])
            return true;
    }
    
    return false;
}

- (void)setTinPosition:(CGPoint)position
{
    self.position = position;
    if (frontSprite){
        frontSprite.position = position;
    }
    if (tinLogo){
        tinLogo.position = CGPointMake(position.x, position.y + 200);
    }
}

@end
