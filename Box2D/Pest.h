//
//  Pest.h
//  Box2D
//
//  Created by Azeem on 28/11/2012.
//
//

#import "cocos2d.h"
#import "Box2D.h"

@interface Pest : CCSprite
{
    BOOL _startChecking;
    CGPoint _endPoint;
    NSString *deadFileName;
}

@property (nonatomic) BOOL checkCollision;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *type;
@property (nonatomic) BOOL isDead;
@property (nonatomic) BOOL shallAddPoint;
@property (nonatomic) b2Body *body;
@property (nonatomic) BOOL isOut;

- (id)initWithFile:(NSString *)filename name:(NSString *)aName andType:(NSString *)aType;
- (void)setEndingPoint:(CGPoint)endPoint;
- (void)die;
- (void)destroyPest;

@end
