//
//  ProgressTimer.h
//  Box2D
//
//  Created by Azeem on 30/11/2012.
//
//

#import "cocos2d.h"
#import "GameScene.h"

@interface ProgressTimer : CCNode
{
    CCProgressTimer *progressTimer;
    CCSprite *glow;
    float time;
}

- (id)initWithTime:(float)time;
- (void)startTimer;
- (void)stopTimer;

@end
