//
//  SummaryLayer.m
//  Box2D
//
//  Created by Azeem on 01/12/2012.
//
//

#import "SummaryLayer.h"
#import "ModalPopup.h"
#import "GameScene.h"

#define kReplayButton 0
#define kNextButton 1

@implementation SummaryLayer

@synthesize popupDelegate;

- (id)initWithStageNum:(int)stage tins:(NSArray *)tins andScore:(int)score
{
    self = [super init];
    if (self)
    {
        CCSprite *background = [CCSprite spriteWithFile:@"Summary.png"];
        [self addChild:background];
        
        self.contentSize = background.contentSize;
        
        Tin *tin1 = [tins objectAtIndex:0];
        Tin *tin2 = [tins objectAtIndex:1];
        Tin *tin3 = [tins objectAtIndex:2];
        
        title = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"STAGE %i SUMMARY", stage + 1] fntFile:@"Title.fnt"];//[CCLabelTTF labelWithString:[NSString stringWithFormat:@"STAGE %i SUMMARY", stage + 1] fontName:@"Soup Of Justice" fontSize:48];
		title.position = ccp(20, 320);
//        title.color = ccc3(235, 111, 63);
		[self addChild:title];
        
        // Set score position
        firstTinScore = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i", tin1.score] fntFile:@"TotalScore.fnt" width:kCCLabelAutomaticWidth alignment:kCCTextAlignmentRight];
        firstTinScore.position = ccp(120, 200);
//        firstTinScore.color = ccc3(250, 138, 76);
        [self addChild:firstTinScore];
        
        secondTinScore = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i", tin2.score] fntFile:@"TotalScore.fnt" width:kCCLabelAutomaticWidth alignment:kCCTextAlignmentRight];
        secondTinScore.position = ccp(120, 22);
//        secondTinScore.color = ccc3(250, 138, 76);
        [self addChild:secondTinScore];
        
        thirdTinScore = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i", tin3.score] fntFile:@"TotalScore.fnt" width:kCCLabelAutomaticWidth alignment:kCCTextAlignmentRight];
        thirdTinScore.position = ccp(120, -160);
//        thirdTinScore.color = ccc3(250, 138, 76);
        [self addChild:thirdTinScore];
        
        firstTinMultiplier = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i", tin1.multiplier] fntFile:@"Multiplier.fnt"];
        firstTinMultiplier.position = ccp(-25, 205);
//        firstTinMultiplier.color = ccc3(246, 196, 70);
        [self addChild:firstTinMultiplier];
        
        secondTinMuliplier = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i", tin2.multiplier] fntFile:@"Multiplier.fnt"];
        secondTinMuliplier.position = ccp(-25, 27);
//        secondTinMuliplier.color = ccc3(246, 196, 70);
        [self addChild:secondTinMuliplier];
        
        thirdTinMultiplier = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i", tin3.multiplier] fntFile:@"Multiplier.fnt"];
        thirdTinMultiplier.position = ccp(-25, -152);
//        thirdTinMultiplier.color = ccc3(246, 196, 70);
        [self addChild:thirdTinMultiplier];
        
        totalScore = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i", score] fntFile:@"TotalScore.fnt" width:kCCLabelAutomaticWidth alignment:kCCTextAlignmentCenter];
        totalScore.position = ccp(125, -308);
//        totalScore.color = ccc3(250, 138, 76);
        [self addChild:totalScore];
        
        CCSprite *firstTinBackground = [CCSprite spriteWithFile:@"tinSummaryBackground.png"];
        firstTinBackground.position = ccp(-180, 228);
        firstTinBackground.scale = 0.45;
        [self addChild:firstTinBackground];
        firstTin = [CCSprite spriteWithTexture:[tin1 texture]];
        firstTin.position = ccp(-180, 188);
        firstTin.scale = 0.6;
        [self addChild:firstTin];
        CCSprite *firstTinLogo = [CCSprite spriteWithFile:[tin1.name stringByReplacingOccurrencesOfString:@"Tin" withString:@"Logo.png"]];
        if (firstTinLogo)
        {
            firstTinLogo.position = ccp(firstTin.position.x, firstTin.position.y + 80);
            firstTinLogo.scale = 0.55;
            [self addChild:firstTinLogo];
        }
        
        CCSprite *secondTinBackground = [CCSprite spriteWithFile:@"tinSummaryBackground.png"];
        secondTinBackground.position = ccp(-180, 40);
        secondTinBackground.scale = 0.45;
        [self addChild:secondTinBackground];
        secondTin = [CCSprite spriteWithTexture:[tin2 texture]];
        secondTin.position = ccp(-180, 0);
        secondTin.scale = 0.6;
        [self addChild:secondTin];
        CCSprite *secondTinLogo = [CCSprite spriteWithFile:[tin2.name stringByReplacingOccurrencesOfString:@"Tin" withString:@"Logo.png"]];
        if (secondTinLogo)
        {
            secondTinLogo.position = ccp(secondTin.position.x, secondTin.position.y + 80);
            secondTinLogo.scale = 0.55;
            [self addChild:secondTinLogo];
        }
        
        CCSprite *thirdTinBackground = [CCSprite spriteWithFile:@"tinSummaryBackground.png"];
        thirdTinBackground.position = ccp(-180, -140);
        thirdTinBackground.scale = 0.45;
        [self addChild:thirdTinBackground];
        thirdTin = [CCSprite spriteWithTexture:[tin3 texture]];
        thirdTin.position = ccp(-180, -180);
        thirdTin.scale = 0.6;
        [self addChild:thirdTin];
        CCSprite *thirdTinLogo = [CCSprite spriteWithFile:[tin3.name stringByReplacingOccurrencesOfString:@"Tin" withString:@"Logo.png"]];
        if (thirdTinLogo)
        {
            thirdTinLogo.position = ccp(thirdTin.position.x, thirdTin.position.y + 80);
            thirdTinLogo.scale = 0.55;
            [self addChild:thirdTinLogo];
        }
        
        firstTinDetail = [CCLabelTTF labelWithString:tin1.description dimensions:CGSizeMake(300, 90) hAlignment:kCCTextAlignmentLeft lineBreakMode:kCCLineBreakModeWordWrap fontName:@"Helvetica" fontSize:13];
        firstTinDetail.color = ccBLACK;
        firstTinDetail.position = ccp(60, 125);
        [self addChild:firstTinDetail];
        
        secondTinDetail = [CCLabelTTF labelWithString:tin2.description dimensions:CGSizeMake(300, 90) hAlignment:kCCTextAlignmentLeft lineBreakMode:kCCLineBreakModeWordWrap fontName:@"Helvetica" fontSize:13];
        secondTinDetail.color = ccBLACK;
        secondTinDetail.position = ccp(60, -53);
        [self addChild:secondTinDetail];
        
        thirdTinDetail = [CCLabelTTF labelWithString:tin3.description dimensions:CGSizeMake(300, 90) hAlignment:kCCTextAlignmentLeft lineBreakMode:kCCLineBreakModeWordWrap fontName:@"Helvetica" fontSize:13];
        thirdTinDetail.color = ccBLACK;
        thirdTinDetail.position = ccp(60, -233);
        [self addChild:thirdTinDetail];
        
        CCSprite *replaySprite = [CCSprite spriteWithFile:@"replayButton.png"];
        replaySprite.position = ccp(- (self.contentSize.width / 2 - 69), self.contentSize.height / 2 - 55);
        replaySprite.scale = 0.9;
        [self addChild:replaySprite];
        
        replayButton = [UIButton buttonWithType:UIButtonTypeCustom];
        replayButton.selected = NO;
        replayButton.tag = kReplayButton;
        replayButton.frame = CGRectMake(275, 29, replaySprite.contentSize.width + 20, replaySprite.contentSize.height + 10);
        [replayButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchDown];
        [replayButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [replayButton addTarget:self action:@selector(buttonCancelled:) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchCancel];
        replayButton.backgroundColor = [UIColor clearColor];
        
        [[[CCDirector sharedDirector] view] addSubview:replayButton];
        
        nextButtonSprite = [CCSprite spriteWithFile:@"nextButton.png"];
        nextButtonSprite.position = ccp(self.contentSize.width / 2 + 70, 0);
        [self addChild:nextButtonSprite];
        
        nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        nextButton.selected = NO;
        nextButton.tag = kNextButton;
        nextButton.frame = CGRectMake(805, 335, nextButtonSprite.contentSize.width + 20, nextButtonSprite.contentSize.height + 10);
        [nextButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchDown];
        [nextButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [nextButton addTarget:self action:@selector(buttonCancelled:) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchCancel];
        nextButton.backgroundColor = [UIColor clearColor];
        
        [[[CCDirector sharedDirector] view] addSubview:nextButton];
    }
    return self;
}

- (void)buttonCancelled:(UIButton *)sender
{
    if (sender.tag == kReplayButton)
    {
        
    }
    else if (sender.tag == kNextButton)
    {
        // Change next button image
        CCSprite *pressedTexture = [CCSprite spriteWithFile:@"nextButton.png"];
        [nextButtonSprite setTexture:pressedTexture.texture];
    }
}

- (void)buttonPressed:(UIButton *)sender
{
    GameScene *gameScene = (GameScene *)[[[CCDirector sharedDirector] runningScene] getChildByTag:kGameLayer];
    if (sender.tag == kReplayButton)
    {
        [popupDelegate closePopup];
        [gameScene replayLevel];
    }
    else if (sender.tag == kNextButton)
    {
        // Change next button image
        CCSprite *pressedTexture = [CCSprite spriteWithFile:@"nextButton.png"];
        [nextButtonSprite setTexture:pressedTexture.texture];
        [popupDelegate closePopup];
        [gameScene nextLevel];
    }
    sender.enabled = false;
}

- (void)buttonTapped:(UIButton *)sender
{
    if (sender.tag == kReplayButton)
    {
        
    }
    else if (sender.tag == kNextButton)
    {
        // Change next button image
        CCSprite *pressedTexture = [CCSprite spriteWithFile:@"nextButtonPressed.png"];
        [nextButtonSprite setTexture:pressedTexture.texture];
    }
}

- (void)onExit
{
    [replayButton removeFromSuperview];
    [nextButton removeFromSuperview];
}

#pragma mark - ModalLayerDelegate Methods
- (BOOL)shallClose
{
    return false;
}

@end
