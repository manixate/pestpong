//
//  Leaderboard.m
//  Box2D
//
//  Created by Azeem on 05/12/2012.
//
//

#import "Leaderboard.h"
#import "GameManager.h"
#import "ModalPopup.h"

@implementation Leaderboard

@synthesize popupDelegate;

- (id)init
{
    self = [super init];
    if (self)
    {
        CCSprite *background = [CCSprite spriteWithFile:@"BasicPopup.png"];
        [self addChild:background];
        
        self.contentSize = background.contentSize;
        
        title = [CCLabelBMFont labelWithString:@"Leaderboard" fntFile:@"Title.fnt"];//[CCLabelTTF labelWithString:@"Leaderboard" fontName:@"Soup Of Justice" fontSize:48];
//        title.color = ccc3(235, 111, 63);
		title.position = ccp(0, 320);
		[self addChild:title];
        
        number = [CCLabelTTF labelWithString:@"Pos" fontName:@"Helvetica" fontSize:24];
        number.anchorPoint = ccp(0, 0.5);
        number.position = ccp(-200, 250);
        number.color = ccc3(60, 60, 60);
        [self addChild:number];
        
        name = [CCLabelTTF labelWithString:@"Name" fontName:@"Helvetica" fontSize:24];
        name.anchorPoint = ccp(0, 0.5);
        name.position = ccp(-110, 250);
        name.color = ccc3(60, 60, 60);
        [self addChild:name];
        
        score = [CCLabelTTF labelWithString:@"Score" fontName:@"Helvetica" fontSize:24];
        score.anchorPoint = ccp(0, 0.5);
        score.position = ccp(140, 250);
        score.color = ccc3(60, 60, 60);
        [self addChild:score];
        
        NSArray *leaderboard = [[[GameManager gameManager] getLeaderBoardScores] retain];
        
        int y = 200;
        int i = 1;
        for (NSDictionary *playerInfo in leaderboard)
        {
            NSString *pName = [playerInfo objectForKey:kFullName];
            NSNumber *pScore = [playerInfo objectForKey:kUserScore];
            
            CCLabelTTF *playerPos = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", i] fontName:@"Helvetica-Bold" fontSize:22];
            playerPos.anchorPoint = ccp(0, 0.5);
            playerPos.position = ccp(-200, y);
            playerPos.color = ccc3(221, 224, 85);
            [self addChild:playerPos];
            
            CCLabelTTF *playerName = [CCLabelTTF labelWithString:pName fontName:@"Helvetica-Bold" fontSize:22];
            playerName.anchorPoint = ccp(0, 0.5);
            playerName.position = ccp(-110, y);
            playerName.color = ccc3(221, 224, 85);
            [self addChild:playerName];
            
            CCLabelTTF *playerScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", pScore.integerValue] fontName:@"Helvetica-Bold" fontSize:22];
            playerScore.anchorPoint = ccp(0, 0.5);
            playerScore.position = ccp(140, y);
            playerScore.color = ccc3(221, 224, 85);
            [self addChild:playerScore];
            
            i++;
            y -= 50;
            
            if (i > 10)
                break;
        }
        
        [leaderboard release];
    }
    return self;
}

#pragma mark - ModalLayerDelegate Methods
- (BOOL)shallClose
{
    return true;
}

@end