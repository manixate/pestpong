//
//  SummaryLayer.h
//  Box2D
//
//  Created by Azeem on 01/12/2012.
//
//

#import "cocos2d.h"
#import "Tin.h"

@protocol ModalLayerDelegate, PopupDelegate;

@interface SummaryLayer : CCNode<ModalLayerDelegate>
{
    CCLabelBMFont *title;
    CCSprite *firstTin;
    CCSprite *secondTin;
    CCSprite *thirdTin;
    CCLabelBMFont *firstTinScore;
    CCLabelBMFont *secondTinScore;
    CCLabelBMFont *thirdTinScore;
    CCLabelBMFont *firstTinMultiplier;
    CCLabelBMFont *secondTinMuliplier;
    CCLabelBMFont *thirdTinMultiplier;
    CCLabelBMFont *totalScore;
    CCLabelBMFont *firstTinDetail;
    CCLabelBMFont *secondTinDetail;
    CCLabelBMFont *thirdTinDetail;
    
    CCSprite *nextButtonSprite;
    UIButton *replayButton;
    UIButton *nextButton;
}

@property (nonatomic, assign) id<PopupDelegate> popupDelegate;

- (id)initWithStageNum:(int)stage tins:(NSArray *)tins andScore:(int)score;

@end
