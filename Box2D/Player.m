//
//  Player.m
//  Box2D
//
//  Created by Azeem on 02/12/2012.
//
//

#import "Player.h"

@implementation Player
@synthesize name, score, email, isSubscribed;

- (id)init
{
    self = [super init];
    if (self) {
        name = @"";
        email = @"";
        isSubscribed = false;
        score = 0;
    }
    return self;
}

@end
