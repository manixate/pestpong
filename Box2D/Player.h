//
//  Player.h
//  Box2D
//
//  Created by Azeem on 02/12/2012.
//
//

#import <Foundation/Foundation.h>

@interface Player : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *email;
@property (nonatomic) bool isSubscribed;
@property (nonatomic) int score;

@end
