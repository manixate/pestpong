//
//  ProgressTimer.m
//  Box2D
//
//  Created by Azeem on 30/11/2012.
//
//

#import "ProgressTimer.h"
#import "GameScene.h"

@implementation ProgressTimer

- (id)initWithTime:(float)t
{
    if (self = [super init])
    {
        CCSprite *outerBar = [CCSprite spriteWithFile:@"ProgressBar.png"];
        [self addChild:outerBar];
        outerBar.position = ccpAdd(outerBar.position, ccp(0, 3));
        
        progressTimer = [CCProgressTimer progressWithSprite:[CCSprite spriteWithFile:@"BlueBar.png"]];
        progressTimer.type = kCCProgressTimerTypeBar;
        progressTimer.midpoint = ccp(0,0); // starts from left
        progressTimer.barChangeRate = ccp(0,1); // grow only in the "x"-horizontal direction
        progressTimer.percentage = 100;
//        progressTimer.position = pos;
        [self addChild:progressTimer];
        
        time = t;
        
        glow = [CCSprite spriteWithFile:@"glowForBar.png"];
        [self addChild:glow];
        
        [self updateGlow];
        
        CCSprite *texture = [CCSprite spriteWithFile:@"Texture.png"];
        [self addChild:texture];
    }
    
    return self;
}

- (void)startTimer
{
    [self updateGlow];
    [self schedule:@selector(updateTimer:) interval:0.5];
}

- (void)stopTimer
{
    [self unschedule:@selector(updateTimer:)];
}

- (void)updateTimer:(ccTime)dt
{
    if (progressTimer.percentage <= 0)
    {
        [self stopTimer];
        GameScene *gameScene = (GameScene *)[[[CCDirector sharedDirector] runningScene] getChildByTag:kGameLayer];
        [gameScene timeUp];
    }
    
    progressTimer.percentage -= (dt / time) * 100;
    [self updateGlow];

    if (progressTimer.percentage <= 50)
    {
        [progressTimer.sprite setTexture:[[CCSprite spriteWithFile:@"YellowBar.png"] texture]];
    }
    
    if (progressTimer.percentage <= 25)
    {
        [progressTimer.sprite setTexture:[[CCSprite spriteWithFile:@"OrangeBar.png"] texture]];
    }
}

- (void)updateGlow
{
    float scaleOfBar = 1 - progressTimer.percentage / 100;
    glow.position = ccp(progressTimer.position.x, (progressTimer.position.y + progressTimer.contentSize.height / 2)- (progressTimer.contentSize.height) * scaleOfBar);
}

@end