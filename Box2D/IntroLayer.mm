//
//  IntroLayer.m
//  Box2D
//
//  Created by Azeem on 03/11/2012.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "IntroLayer.h"
#import "ModalPopup.h"
#import "AppDelegate.h"
#import "GameScene.h"
#import "SummaryLayer.h"
#import "LegalLayer.h"
#import "SelectionLayer.h"

#pragma mark - IntroLayer

#define kBackground         -1
#define kHeadline           1
#define kFrontLayer         2
#define kAnimatedLayers     3

#define kLeftEyeBrow    10
#define kRightEyeBrow   11
#define kSlime          12

@interface IntroLayer ()
{

}

- (void)addTitleAtPosition:(CGPoint)pos;
- (void)addGrassAtPosition:(CGPoint)pos;
- (void)addVirusAtPosition:(CGPoint)pos;
- (void)addMenuAtPosition:(CGPoint)pos;

@end

// HelloWorldLayer implementation
@implementation IntroLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	IntroLayer *layer = [IntroLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init
{
    self = [super init];
    if (self) {
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Images.plist"];
        
        CCSpriteBatchNode *spriteBatchNode = [CCSpriteBatchNode batchNodeWithFile:@"Images.png"];
        
        [self addChild:spriteBatchNode];
    }
    return self;
}

-(void) onEnter
{
	[super onEnter];

    GameManager *manager = [GameManager gameManager];
    [manager resetGame];
    
	// ask director for the window size
	screenSize = [[CCDirector sharedDirector] winSize];

	CCSprite *background;
	
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
		background = [CCSprite spriteWithFile:@"HomeScreenBackground.png"];
		background.rotation = 90;
	} else {
		background = [CCSprite spriteWithFile:@"HomeScreenBackground.png"];
	}
	background.position = ccp(screenSize.width/2, screenSize.height/2);

	// add the label as a child to this Layer
	[self addChild: background z:kBackground];
	
	// In one second transition to the new scene
	//[self scheduleOnce:@selector(makeTransition:) delay:1];
    
    // Add Title
    [self addTitleAtPosition:ccp(screenSize.width/2, screenSize.height/1.5)];
    
    // Add Menu
    [self addMenuAtPosition:ccp(screenSize.width/2, 40)];
    
    // Add Grass
    [self addGrassAtPosition:ccp(screenSize.width/2 + 20, screenSize.height/3.5)];
    
    // Add Insect
    [self addInsectAtPosition:ccp(screenSize.width, screenSize.height/3)];
    
    // Add Virus
    [self addVirusAtPosition:ccp(screenSize.width/7, screenSize.height/2.5)];
    
    if ([[GameManager gameManager] currentScenario] == -1)
    {
        // Show selection screen
        SelectionLayer *selectionLayer = [[[SelectionLayer alloc] init] autorelease];
        ModalPopup *popup = [[[ModalPopup alloc] initWithNode:selectionLayer] autorelease];
        popup.pauseLayerDelegate = self;
        popup.modalLayerDelegate = selectionLayer;
        selectionLayer.popupDelegate = popup;
        [self addChild:popup z:INT_MAX];
    }
    else
    {
        // Play Sound
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        // Play Sound
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"GameShowFever.mp3" loop:NO];
    }
}

- (void)addTitleAtPosition:(CGPoint)pos
{
    CCSprite *logo = [CCSprite spriteWithFile:@"Logo.png"];
    logo.position = ccp(pos.x, pos.y + 180);
    [self addChild:logo z:kHeadline];
    
    CCSprite *headline = [CCSprite spriteWithSpriteFrameName:@"Headline.png"];//[CCSprite spriteWithFile:@"Headline.png"];
    headline.scale = 0.1;
    headline.position = pos;
    
    [self addChild:headline z:kHeadline];
    
    CCAction *entrance = [CCScaleTo actionWithDuration:0.5 scale:1];
    [headline runAction:entrance];
    
    CCSprite *slime = [CCSprite spriteWithSpriteFrameName:@"Slime.png"];
    slime.scale = 0;
    slime.position = ccp(pos.x * 2, pos.y * 2);
    
    [self addChild:slime z:kFrontLayer tag:kSlime];
    
    CCSpawn *slimeSpawn = [CCSpawn actions:[CCScaleTo actionWithDuration:0.5 scale:1.0], [CCMoveTo actionWithDuration:0.5 position:ccp(pos.x * 1.5, pos.y)], nil];
    CCSequence *slimeSequence = [CCSequence actions:[CCDelayTime actionWithDuration:0.5], slimeSpawn, [CCDelayTime actionWithDuration:5], [CCFadeOut actionWithDuration:5.0], [CCCallFunc actionWithTarget:slime selector:@selector(removeFromParentAndCleanup:)], nil];
    
    [slime runAction:slimeSequence];
}

- (void)addGrassAtPosition:(CGPoint)pos
{
    // Add body
    CCSprite *grassSprite = [CCSprite spriteWithSpriteFrameName:@"GreenGrassBody.png"];
    grassSprite.position = ccp(pos.x, pos.y);
    
    [self addChild:grassSprite z:kFrontLayer];
    
    // Add animating eyebrows
    CCSprite *leftEyeBrow = [CCSprite spriteWithSpriteFrameName:@"LeftEyeBrow.png"];
    leftEyeBrow.position = ccp(pos.x - 15, pos.y + 55);
    
    [self addChild:leftEyeBrow z:kAnimatedLayers tag:kLeftEyeBrow];
    
    CCSprite *rightEyeBrow = [CCSprite spriteWithSpriteFrameName:@"RightEyeBrow.png"];
    rightEyeBrow.position = ccp(pos.x + 45, pos.y + 55);
    
    [self addChild:rightEyeBrow z:kAnimatedLayers tag:kRightEyeBrow];
    
    CCSequence *leftEyeBrowRotationSequence = [CCSequence actionWithArray:[NSArray arrayWithObjects:[CCDelayTime actionWithDuration:0.5], [CCRotateBy actionWithDuration:0.8 angle:-60], [CCRotateBy actionWithDuration:0.8 angle:60], nil]];
    CCRepeatForever *leftEyeBrowRotationRepeat = [CCRepeatForever actionWithAction:leftEyeBrowRotationSequence];
    [leftEyeBrow runAction:leftEyeBrowRotationRepeat];
    
    CCSequence *rightEyeBrowRotationSequence = [CCSequence actionWithArray:[NSArray arrayWithObjects:[CCDelayTime actionWithDuration:0.5], [CCRotateBy actionWithDuration:0.8 angle:60], [CCRotateBy actionWithDuration:0.8 angle:-60], nil]];
    CCRepeatForever *rightEyeBrowRotationRepeat = [CCRepeatForever actionWithAction:rightEyeBrowRotationSequence];
    [rightEyeBrow runAction:rightEyeBrowRotationRepeat];
}

- (void)addInsectAtPosition:(CGPoint)pos
{
    // Add body
    CCSprite *insectSprite = [CCSprite spriteWithFile:@"GreenInsectBody.png"];
    insectSprite.position = ccp(pos.x - insectSprite.contentSize.width/2, pos.y);
    
    [self addChild:insectSprite z:kFrontLayer];
    
    // Add animating eyebrows
    CCSprite *leftEye = [CCSprite spriteWithFile:@"LeftEyeInsect.png"];
    leftEye.position = ccp((pos.x - insectSprite.contentSize.width/2) - 2, pos.y + 85);
    
    [self addChild:leftEye z:kAnimatedLayers];
    
    CCSprite *rightEye = [CCSprite spriteWithFile:@"RightEyeInsect.png"];
    rightEye.position = ccp((pos.x - insectSprite.contentSize.width/2) + 40, pos.y + 118);
    
    [self addChild:rightEye z:kAnimatedLayers];
    
    // Animate
    [leftEye runAction:[CCRepeatForever actionWithAction:[CCSequence actions:[CCScaleTo actionWithDuration:0.5 scale:0.5], [CCScaleTo actionWithDuration:0.5 scale:1], nil]]];
    
    [rightEye runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.5] two:[CCCallBlockN actionWithBlock:^(CCNode *node) {
        [node runAction:[CCRepeatForever actionWithAction:[CCSequence actions:[CCScaleTo actionWithDuration:0.5 scale:0.5], [CCScaleTo actionWithDuration:0.5 scale:1], nil]]];
    }]]];
}

- (void)addVirusAtPosition:(CGPoint)pos
{
    // Add body
    CCSprite *virusSprite = [CCSprite spriteWithSpriteFrameName:@"RedVirusBody.png"];
    virusSprite.position = ccp(pos.x, pos.y);
    
    [self addChild:virusSprite z:kFrontLayer];
    
    // Add animating eyebrows
    CCSprite *leftEyeBall = [CCSprite spriteWithFile:@"LeftEyeDisease.png"];
    leftEyeBall.position = ccp(pos.x - 8, pos.y + 50);
    
    [self addChild:leftEyeBall z:kAnimatedLayers];
    
    CCSprite *rightEyeBall = [CCSprite spriteWithFile:@"RightEyeDisease.png"];
    rightEyeBall.position = ccp(pos.x + 30, pos.y + 20);
    
    [self addChild:rightEyeBall z:kAnimatedLayers];
    
    [leftEyeBall runAction:[CCRepeatForever actionWithAction:[CCRotateBy actionWithDuration:0.8 angle:360]]];
    [rightEyeBall runAction:[CCRepeatForever actionWithAction:[CCRotateBy actionWithDuration:0.8 angle:360]]];
}

- (void)addMenuAtPosition:(CGPoint)pos
{
    CCLayer *menuLayer = [[[CCLayer alloc] init] autorelease];
    [self addChild:menuLayer z:kFrontLayer];
    
    // Add LeaderBoard Button
    CCMenuItemSprite *leaderBoardItem = [[[CCMenuItemSprite alloc] initWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"LeaderboardButton.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"LeaderboardPressedButton.png"] disabledSprite:nil block:^(id sender) {

        Leaderboard *leaderboardLayer = [[[Leaderboard alloc] init] autorelease];
        ModalPopup *popup = [[ModalPopup alloc] initWithNode:leaderboardLayer];
        popup.modalLayerDelegate = leaderboardLayer;
        popup.pauseLayerDelegate = self;
        leaderboardLayer.popupDelegate = popup;
        [self addChild: popup z:INT_MAX];
    }] autorelease];
    
    // Add Rules Button
    CCMenuItemSprite *rulesItem = [[[CCMenuItemSprite alloc] initWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"RulesButton.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"RulesPressedButton.png"] disabledSprite:nil block:^(id sender) {
        
        RulesLayer *rulesLayer = [[[RulesLayer alloc] init] autorelease];
        ModalPopup *popup = [[ModalPopup alloc] initWithNode:rulesLayer];
        rulesLayer.popupDelegate = popup;
        popup.modalLayerDelegate = rulesLayer;
        popup.pauseLayerDelegate = self;
        [self addChild: popup z:INT_MAX];
    }] autorelease];
    
    // Add Play Button
    CCMenuItemSprite *playItem = [[[CCMenuItemSprite alloc] initWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"PlayButton.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"PlayPressedButton.png"] disabledSprite:nil block:^(id sender) {
        
        [self makeTransition:0];
        
    }] autorelease];
    
    // Add Legal Button
    CCMenuItemSprite *legalItem = [[[CCMenuItemSprite alloc] initWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"LegalButton.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"LegalPressedButton.png"] disabledSprite:nil block:^(id sender) {

        LegalLayer *legalLayer = [[[LegalLayer alloc] init] autorelease];
        ModalPopup *popup = [[ModalPopup alloc] initWithNode:legalLayer];
        popup.modalLayerDelegate = legalLayer;
        popup.pauseLayerDelegate = self;
        legalLayer.popupDelegate = popup;
        [self addChild: popup z:INT_MAX];
    }] autorelease];
    
    // Add Menu
    CCMenu *menu = [CCMenu menuWithItems:leaderBoardItem, rulesItem, playItem, legalItem, nil];
    [menu alignItemsHorizontallyWithPadding:50];
    menu.position = ccp(pos.x, pos.y);
    [menuLayer addChild:menu];
}

-(void) makeTransition:(ccTime)dt
{
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameScene scene] withColor:ccWHITE]];
}

#pragma mark - GKLeaderBoardViewControllerDelegate Methods
- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

#pragma mark - PauseLayerDelegate
- (void)pause
{
    // Pause all Animations
    CCSprite *slime = (CCSprite *)[self getChildByTag:kSlime];
    [slime pauseSchedulerAndActions];
    
    CCSprite *leftEyeBrow = (CCSprite *)[self getChildByTag:kLeftEyeBrow];
    [leftEyeBrow pauseSchedulerAndActions];
    
    CCSprite *rightEyeBrow = (CCSprite *)[self getChildByTag:kRightEyeBrow];
    [rightEyeBrow pauseSchedulerAndActions];
}

- (void)resume
{
    // Resume all Animations
    CCSprite *slime = (CCSprite *)[self getChildByTag:kSlime];
    [slime resumeSchedulerAndActions];
    
    CCSprite *leftEyeBrow = (CCSprite *)[self getChildByTag:kLeftEyeBrow];
    [leftEyeBrow resumeSchedulerAndActions];
    
    CCSprite *rightEyeBrow = (CCSprite *)[self getChildByTag:kRightEyeBrow];
    [rightEyeBrow resumeSchedulerAndActions];
}

@end
