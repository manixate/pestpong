//
//  MyContactListener.h
//  Box2DPong
//
//  Created by Ray Wenderlich on 2/18/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#import "Box2D.h"
#import <vector>
#import <algorithm>

struct MyContact {
    b2Fixture *fixtureA;
    b2Fixture *fixtureB;
    bool operator==(const MyContact& other) const
    {
        return (fixtureA == other.fixtureA) && (fixtureB == other.fixtureB);
    }
};

struct Fixture
{
    b2Fixture *fixture;
    float height;
    
    Fixture(b2Fixture *f, float h)
    {
        fixture = f;
        height = h;
    }
};
    
class MyContactListener : public b2ContactListener {

public:
    std::vector<MyContact>_contacts;
    b2Fixture *m_platformFixture;
    b2Body *m_outSensor;
    b2Body *m_sensor;
    b2Body *m_outFixture;
    std::vector<Fixture>platforms;
    bool stopScaling;
    
    MyContactListener();
    ~MyContactListener();
    
	virtual void BeginContact(b2Contact* contact);
	virtual void EndContact(b2Contact* contact);
	virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);    
	virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
};
