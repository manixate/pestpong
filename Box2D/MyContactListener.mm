//
//  MyContactListener.m
//  Box2DPong
//
//  Created by Ray Wenderlich on 2/18/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#import "MyContactListener.h"
#import "Pest.h"
#import "Tin.h"
#import "GameScene.h"

MyContactListener::MyContactListener() : _contacts(), stopScaling(true), platforms() {
}

MyContactListener::~MyContactListener() {
}

void MyContactListener::BeginContact(b2Contact* contact) {
    b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();
    
    //check if one of the fixtures is the platform
    b2Fixture* platformFixture = NULL;
    float platformHeight = 0;
    b2Fixture* otherFixture = NULL;
    BOOL isOutFixture = false;
    
    // Check if we have collided with outer fixture
    if (m_outFixture == fixtureA->GetBody())
    {
        isOutFixture = true;
        otherFixture = fixtureB;
    }
    else if (m_outFixture == fixtureB->GetBody())
    {
        isOutFixture = true;
        otherFixture = fixtureA;
    }
    
    if (isOutFixture)
    {
        // Assuming that pest is the other fixture
        Pest *pest = static_cast<Pest *>(otherFixture->GetBody()->GetUserData());
        //        if (otherFixture->GetBody()->GetLinearVelocity().y > 0)
        if (!(pest.isOut))
        {
            contact->SetEnabled(false);
            return;
        }
        
        return;
    }
    
    // Check if it is in our platform fixtures or not
    for (std::vector<Fixture>::iterator itr = platforms.begin();
         itr != platforms.end(); itr++)
    {
        if (itr->fixture == fixtureA)
        {
            platformFixture = fixtureA;
            platformHeight = itr->height;
            otherFixture = fixtureB;
            
            break;
        }
        else if (itr->fixture == fixtureB)
        {
            platformFixture = fixtureB;
            platformHeight = itr->height;
            otherFixture = fixtureA;
            
            break;
        }
    }
    
    if ( !platformFixture )
        return;
    
    b2Body* platformBody = platformFixture->GetBody();
    b2Body* otherBody = otherFixture->GetBody();
    
    // Assuming that other body will always be pest
    Pest *pest = (Pest *)(otherBody->GetUserData());
    if (!(pest.checkCollision) || pest.isOut)
    {
        contact->SetEnabled(false);
        return;
    }
    
    // Check if we hit the bottom of the basket
    if (pest.shallAddPoint && platformFixture->GetUserData())
    {
        CCSprite *sprite = (CCSprite *)platformFixture->GetUserData();
        if ([sprite isKindOfClass:[Tin class]])
        {
            Tin *tin = (Tin *)platformFixture->GetUserData();
            NSLog(@"Tin: %@", tin.name);
            
            GameScene *gameScene = [[[[CCDirector sharedDirector] runningScene] children] lastObject];
            //[gameScene reorderChild:pest z:kInTin];
            
            // Scoring
            if ([tin isAllowed:pest.name])
                [gameScene drawPlusPoint:tin];
            else
                [gameScene drawMinusPoint:tin];
            
            [gameScene splashTin:tin];
            [pest die];
            pest.shallAddPoint = NO;
            
            // Randomize tins if possible
            [gameScene randomizeTins];
        }
    }
    
    if (!pest.isDead)
    {
        [pest die];
    }
    
    /*int numPoints = contact->GetManifold()->pointCount;
    b2WorldManifold worldManifold;
    contact->GetWorldManifold( &worldManifold );
    
    // Check if we should handle the collision or not
//    NSLog(@"%f < %f", otherBody->GetPosition().y, m_sensor->GetPosition().y);
//    if (otherBody->GetPosition().y < m_sensor->GetPosition().y)
//    {
//        contact->SetEnabled(NO);
//        return;
//    }
    
    //check if contact points are moving into platform
    for (int i = 0; i < numPoints; i++) {
        
        b2Vec2 pointVelPlatform =
        platformBody->GetLinearVelocityFromWorldPoint( worldManifold.points[i] );
        b2Vec2 pointVelOther =
        otherBody->GetLinearVelocityFromWorldPoint( worldManifold.points[i] );
        b2Vec2 relativeVel = platformBody->GetLocalVector( pointVelOther - pointVelPlatform );
        CCLOG(@"Check");
        if ( relativeVel.y < -1 ) //if moving down faster than 1 m/s, handle as before
        {
            stopScaling = true;
            return;//point is moving into platform, leave contact solid and exit
        }
        else if ( relativeVel.y < 1 )
        {   //if moving slower than 1 m/s
            //borderline case, moving only slightly out of platform
            b2Vec2 relativePoint = platformBody->GetLocalPoint( worldManifold.points[i] );
            float platformFaceY = platformHeight;//front of platform, from fixture definition :(
            if ( relativePoint.y > platformFaceY - 0.05 )
            {
                CCLOG(@"Check ittt: %f, %f\n\n", relativePoint.y, platformFaceY);
                stopScaling = true;
                return;//contact point is less than 5cm inside front face of platfrom
            }
            CCLOG(@"Check Not: %f, %f\n\n", relativePoint.y, platformFaceY);
            
            if (relativePoint.x > 0 - 0.05 || relativePoint.x < 0 - 0.05)
            {
                return;
            }
        }
        else
            ;//moving up faster than 1 m/s
    }*/
    //return;
    //no points are moving into platform, contact should not be solid
    //contact->SetEnabled(false);
}

void MyContactListener::EndContact(b2Contact* contact)
{
    b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();
    
    //check if one of the fixtures is the platform
    b2Fixture* otherFixture = NULL;
    bool isSensor = false;
    bool isOutSensor = false;
    
    if (m_sensor == fixtureA->GetBody())
    {
        isSensor = true;
        otherFixture = fixtureB;
    }
    else if (m_sensor == fixtureB->GetBody())
    {
        isSensor = true;
        otherFixture = fixtureA;
    }
    else if (m_outSensor == fixtureA->GetBody())
    {
        isOutSensor = true;
        otherFixture = fixtureB;
    }
    else if (m_outSensor == fixtureB->GetBody())
    {
        isOutSensor = true;
        otherFixture = fixtureA;
    }
    
    if (isSensor)
    {
        Pest *pest = static_cast<Pest *>(otherFixture->GetBody()->GetUserData());
        pest.checkCollision = true;
        
        if (otherFixture->GetBody()->GetLinearVelocity().y > 0)
        {
            GameScene *gameScene = [[[[CCDirector sharedDirector] runningScene] children] lastObject];
            [gameScene reorderChild:pest z:kInTin];
        }
    }
    else if (isOutSensor)
    {
        Pest *pest = static_cast<Pest *>(otherFixture->GetBody()->GetUserData());
        if (otherFixture->GetBody()->GetLinearVelocity().y > 0)
        {
            pest.isOut = true;
            
            [pest runAction:[CCScaleTo actionWithDuration:0.5 scale:0.8]];
            GameScene *gameScene = [[[[CCDirector sharedDirector] runningScene] children] lastObject];
            [gameScene reorderChild:pest z:kBehindPlatform];
        }
    }
    
    //reset the default state of the contact in case it comes back for more
    contact->SetEnabled(true);
}

void MyContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
}

void MyContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
}

