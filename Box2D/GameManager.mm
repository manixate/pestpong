//
//  GameManager.m
//  Box2D
//
//  Created by Azeem on 02/12/2012.
//
//

#import "GameManager.h"
#import "JSONKit.h"
#import "AppDelegate.h"

#define kWarning 1
#define kPasswordPrompt 2
#define PASSWORD @"1234"

static GameManager *gameManager = nil;

@implementation GameManager

@synthesize currentLevel, score, currentScenario, player, pestTimeOut, levelTimeOut;

+ (id)gameManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gameManager = [[GameManager alloc] init];
    });
    
    return gameManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        player = nil;
        currentLevel = 0;
        score = 0;
        currentScenario = -1;
        
        password = nil;
        [self loadData];
    }
    return self;
}

- (void)downloadGameData
{
    NSError *error = nil;
    NSString *dataString = [NSString stringWithContentsOfURL:[NSURL URLWithString:kDataGetURL] encoding:NSUTF8StringEncoding error:&error];
    if (error)
    {
        password = @"12345";
        pestTimeOut = 3;
        levelTimeOut = 60;
    }
    else
    {
        NSDictionary *dataDict = [[JSONDecoder decoder] objectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding]];
        password = [[dataDict objectForKey:kPasswordKey] retain];
        pestTimeOut = [[dataDict objectForKey:kPestTimeoutKey] floatValue];
        levelTimeOut = [[dataDict objectForKey:kLevelTimeKey] floatValue];
    }
}

- (void)loadData
{
    if (!player)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Password" message:@"Please Enter Password" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
        alertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
        alertView.tag = kPasswordPrompt;
        
        [alertView show];
        
        return;
    }
}

- (NSDictionary *)getDataForScenario:(int)aScenario andLevel:(int)aLevel
{
    NSDictionary *dictionary = [[JSONDecoder decoder] objectWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DefaultData" ofType:@"json"]]];
    if (!dictionary)
        return nil;
    
    NSArray *scenarios = [dictionary objectForKey:@"Scenario"];
    NSArray *levels = [[scenarios objectAtIndex:aScenario] objectForKey:@"Levels"];
    
    if (aLevel >= levels.count)
        return nil;
    
    return [levels objectAtIndex:aLevel];
}

- (void)resetGame
{
    currentLevel = 0;
    score = 0;
}

- (NSArray *)getLeaderBoardScores
{
    NSArray *array = nil;
    NSError *error = nil;
    
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:kLeaderboardGetURL, currentScenario + 1]] options:NSDataReadingMappedIfSafe error:&error];
    
    if (error)
    {
        NSLog(@"Error while sending data :%@", error);
        return nil;
    }
    else
    {
        array = [[JSONDecoder decoder] objectWithData:data error:&error];
        if (error)
        {
            NSLog(@"Error while decoding data :%@", error);
            return nil;
        }
    }
    
    return array;
}

- (void)moveToNextLevel
{
    currentLevel++;
    if (currentLevel > 1)
        currentLevel = -1;
//    if (![self getDataForScenario:currentScenario andLevel:currentLevel])
//    {
//        currentLevel = -1;
//    }
}

#pragma mark - UIAlertViewDelegate Methods
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (alertView.tag == kPasswordPrompt)
    {
        if (![[alertView textFieldAtIndex:0] text])
            return false;
        if ([[[alertView textFieldAtIndex:0] text] isEqualToString:@""])
            return false;
    }
    
    return true;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kPasswordPrompt)
    {
        if (alertView.cancelButtonIndex == buttonIndex)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You must register for playing this game" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            alert.tag = kWarning;
            
            [alert show];
            return;
        }
        
        if (!password)
            [self downloadGameData];
        
        NSString *pass = [[alertView textFieldAtIndex:0] text];
        // If password is not correct
        if (![pass isEqualToString:password])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Wrong Password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            alert.tag = kWarning;
            
            [alert show];
            return;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:@"You have successfully logged in. Enjoy the game :)" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            player = [[Player alloc] init];
        }
    }
    else if (alertView.tag == kWarning)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password" message:@"Please Enter Password" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
        alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        alert.tag = kPasswordPrompt;
        
        [alert show];
    }
}

- (void)sendScore
{
    // Formatter set to proper locale by default
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *accessURLString = [[NSString stringWithFormat:kLeaderboardPostURL, player.name, player.email, score, [formatter stringFromDate:[NSDate date]], currentScenario + 1] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Sending Score Using: %@", accessURLString);
    NSError *error = nil;
    [NSString stringWithContentsOfURL:[NSURL URLWithString:accessURLString] encoding:NSUTF8StringEncoding error:&error];
    
//    NSURL *accessURL = [NSURL URLWithString:accessURLString];
//    NSURLRequest *request = [NSURLRequest requestWithURL:accessURL];
//    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)subscribeWithName:(NSString *)name andEmail:(NSString *)email
{
    player.name = [name retain];
    player.email = [email retain];
    player.isSubscribed = true;
}

#pragma mark - Dealloc
- (void)dealloc
{
    [player release];
    [password release];
    [super dealloc];
}

@end