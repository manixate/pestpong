//
//  IntroLayer.h
//  Box2D
//
//  Created by Azeem on 03/11/2012.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "ModalPopup.h"
#import "Leaderboard.h"
#import "RulesLayer.h"

// HelloWorldLayer
@interface IntroLayer : CCLayer <GKLeaderboardViewControllerDelegate, PauseLayerDelegate>
{
    CGSize screenSize;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
