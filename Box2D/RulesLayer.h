//
//  RulesLayer.h
//  Box2D
//
//  Created by Azeem on 05/12/2012.
//
//

#import "cocos2d.h"

@protocol ModalLayerDelegate, PopupDelegate;

@interface RulesLayer : CCNode<ModalLayerDelegate>
{
    UIButton *closeButton;
}

@property (nonatomic, assign) id<PopupDelegate> popupDelegate;

@end
