//
//  GameScene.m
//  Box2D
//
//  Created by Azeem on 17/11/2012.
//
//

#import "GameScene.h"
#import "Pest.h"
#import "Tin.h"
#import "ProgressTimer.h"
#import "ModalPopup.h"
#import "SummaryLayer.h"
#import "LegalLayer.h"
#import "Leaderboard.h"
#import "IntroLayer.h"
#import "CCNode+SFGestureRecognizers.h"
#import <vector.h>

#pragma mark - Implementation
@implementation GameScene

+ (CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameScene *layer = [GameScene node];
	
	// add layer as a child to scene
	[scene addChild:layer z:0 tag:kGameLayer];
	
	// return the scene
	return scene;
}

- (id)init
{
    self = [super init];
    if (self) {
        screenSize = [[CCDirector sharedDirector] winSize];
        
        self.isTouchEnabled = YES;
        
        isPaused = false;
        
        hudLayer = [[CCLayer alloc] init];
        hudLayer.position = ccp(0, 0);
        [self addChild:hudLayer z:kHudLayer tag:kHUD];
        
        GameManager *gameManager = [GameManager gameManager];
        
        gameData = [[gameManager getDataForScenario:gameManager.currentScenario andLevel:gameManager.currentLevel] retain];
        
        // Fill with names of supported pests
        pests = [[gameData objectForKey:@"Pests"] retain];//[[NSArray alloc] initWithObjects:@"Alion", @"ConvergeXT", @"ViosG3", @"Titan", @"Luna", @"Prosaro", nil];
        
        // Pest list in next bar
        pestList = [[NSMutableArray alloc] init];
        
        // Set Selected Sprite
        selectedPest = nil;
        
        isTimeOver = false;
        
        tins = nil;
//        time = [[gameData objectForKey:@"Time"] floatValue];
        time = gameManager.levelTimeOut;

//        pestTimeout = [[gameData objectForKey:@"PestTimeout"] floatValue];
        pestTimeout = gameManager.pestTimeOut;
        gameType = [[gameData objectForKey:@"GameType"] retain];
        
        // Create Physics
        [self initPhysics];
        
        // Create Background
        [self createBackground];
        
        // Create Moving Clouds
        [self createMovingClouds];
        
        // Create Platform to place the tins
        [self createPlatform];
        
        // Create Tins
        [self createTins];
        // Draw Tins
        [self drawTins];
        
        // Create Hand
        //[self createMovingHand];
        
        // Create HUD
        [self createHUD];
        
        // Fill pest list randomly
        [self fillBugsInNextBar];
        
        // Draw next bar
        [self drawNextBar];
        
        // Draw Debug
//        debugDraw = [B2DebugDrawLayer layerWithWorld:world andPtmRatio:PTM_RATIO];
//        [self addChild:debugDraw z:kDebugLayer];
        
        // Preload Audio Files
        [self preloadSounds];
        
        // Schedule Update
        [self scheduleUpdate];
    }
    return self;
}

-(void) initPhysics
{
	b2Vec2 gravity;
	gravity.Set(0.0f, -10.0f);
	world = new b2World(gravity);
	
	// Do we want to let bodies sleep?
	world->SetAllowSleeping(true);
	
	world->SetContinuousPhysics(true);
    
    contactListener = new MyContactListener();
    world->SetContactListener(contactListener);
	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	b2Body* groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;
	
	// bottom
	groundBox.Set(b2Vec2(0,0), b2Vec2(screenSize.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// top
	groundBox.Set(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// left
	groundBox.Set(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(0,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// right
	groundBox.Set(b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
    
    // Create a mid line
    b2BodyDef midBodyDef;
    midBodyDef.position.Set(0, screenSize.height/2.15/PTM_RATIO);
    
    b2PolygonShape sensorBodyMid;
    b2Vec2 vec4[4];
    vec4[0] = b2Vec2(0,0);
    vec4[1] = b2Vec2(screenSize.width/PTM_RATIO, 0);
    vec4[2] = b2Vec2(screenSize.width/PTM_RATIO, 0.4);
    vec4[3] = b2Vec2(0, 0.4);
    sensorBodyMid.Set(vec4, 4);
    
    b2FixtureDef fixtureDef;
    fixtureDef.isSensor = true;
    fixtureDef.shape = &sensorBodyMid;
    
    b2Body *midBody = world->CreateBody(&midBodyDef);
    midBody->CreateFixture(&fixtureDef);
    
    contactListener->m_sensor = midBody;
    
    // Create a Lower Fail Safe
    b2BodyDef failSafeLBodyDef;
    failSafeLBodyDef.type = b2_staticBody;
    failSafeLBodyDef.position.Set(0, screenSize.height/4.5/PTM_RATIO);
    
    b2PolygonShape failSafeLBodyMid;
    vec4[0] = b2Vec2(0, 0);
    vec4[1] = b2Vec2(screenSize.width/PTM_RATIO, 0);
    vec4[2] = b2Vec2(screenSize.width/PTM_RATIO, 1);
    vec4[3] = b2Vec2(0, 1);
    failSafeLBodyMid.Set(vec4, 4);
    
    b2FixtureDef failSafeLFixtureDef;
    failSafeLFixtureDef.shape = &failSafeLBodyMid;
    
    b2Body *failSafeLBody = world->CreateBody(&failSafeLBodyDef);
    b2Fixture *tempL = failSafeLBody->CreateFixture(&failSafeLFixtureDef);
    contactListener->platforms.push_back(Fixture(tempL, 2));
    
    // Create a upper mid line
    b2BodyDef midBodyUDef;
    midBodyUDef.position.Set(0, screenSize.height/1.3/PTM_RATIO);
    
    b2PolygonShape sensorBodyUMid;
    vec4[0] = b2Vec2(0,0);
    vec4[1] = b2Vec2(screenSize.width/PTM_RATIO, 0);
    vec4[2] = b2Vec2(screenSize.width/PTM_RATIO, 1.2);
    vec4[3] = b2Vec2(0, 1.2);
    sensorBodyUMid.Set(vec4, 4);
    
    b2FixtureDef fixtureUDef;
    fixtureUDef.isSensor = true;
    fixtureUDef.shape = &sensorBodyUMid;
    
    b2Body *midUBody = world->CreateBody(&midBodyUDef);
    midUBody->CreateFixture(&fixtureUDef);
    
    contactListener->m_outSensor = midUBody;
    
    // Create a Upper Fail Safe
    b2BodyDef failSafeUBodyDef;
    failSafeUBodyDef.type = b2_staticBody;
    failSafeUBodyDef.position.Set(0, screenSize.height/3.5/PTM_RATIO);
    
    b2PolygonShape failSafeUBodyMid;
    vec4[0] = b2Vec2(0, 0);
    vec4[1] = b2Vec2(screenSize.width/PTM_RATIO, 0);
    vec4[2] = b2Vec2(screenSize.width/PTM_RATIO, 1);
    vec4[3] = b2Vec2(0, 1);
    failSafeUBodyMid.Set(vec4, 4);
    
    b2FixtureDef failSafeUFixtureDef;
    failSafeUFixtureDef.shape = &failSafeUBodyMid;
    
    b2Body *failSafeUBody = world->CreateBody(&failSafeUBodyDef);
    failSafeUBody->CreateFixture(&failSafeUFixtureDef);
    contactListener->m_outFixture = failSafeUBody;
}

- (void)createBackground
{
    CCSprite *backgroundSprite = [CCSprite spriteWithFile:@"GameSceneBackground.png"];
    backgroundSprite.position = ccp(screenSize.width/2, screenSize.height/2);
    
    [self addChild:backgroundSprite z:kBackground tag:kBackgroundSprite];
}

- (void)createMovingClouds
{
    float cloudMovingSpeed = 60;
    
    CCSprite *cloudSprite1 = [CCSprite spriteWithFile:@"MovingClouds.png"];
    cloudSprite1.position = ccp(-screenSize.width/2, screenSize.height/1.4);
    
    [self addChild:cloudSprite1 z:kMovingClouds tag:kCloud1];
    
    [cloudSprite1 runAction:[CCRepeatForever actionWithAction:[CCSequence actionOne:[CCMoveTo actionWithDuration:cloudMovingSpeed position:ccp(screenSize.width/2 + screenSize.width, screenSize.height/1.4)] two:[CCCallBlockN actionWithBlock:^(CCNode *node) {
        cloudSprite1.position = ccp(-screenSize.width/2, screenSize.height/1.4);
    }]]]];
    
    CCSprite *cloudSprite2 = [CCSprite spriteWithFile:@"MovingClouds.png"];
    cloudSprite2.position = ccp(-screenSize.width/2, screenSize.height/1.4);
    
    [self addChild:cloudSprite2 z:kMovingClouds tag:kCloud2];
    
    [cloudSprite2 runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:cloudMovingSpeed / 2] two:[CCCallBlockN actionWithBlock:^(CCNode *node) {
        [cloudSprite2 runAction:[CCRepeatForever actionWithAction:[CCSequence actionOne:[CCMoveTo actionWithDuration:cloudMovingSpeed position:ccp(screenSize.width/2 + screenSize.width, screenSize.height/1.4)] two:[CCCallBlockN actionWithBlock:^(CCNode *node) {
            cloudSprite2.position = ccp(-screenSize.width/2, screenSize.height/1.4);
        }]]]];
    }]]];
}

- (void)createPlatform
{
    CCSprite *platform = [CCSprite spriteWithFile:@"Platform.png"];
    platform.position = ccp(screenSize.width/2, screenSize.height/2.2);
    
    [self addChild:platform z:kPlatform tag:kPlatformSprite];
}

- (void)createTinBodyOfTin:(Tin *)tin
{
    b2PolygonShape polygonShape;
    b2Vec2 vert[4];
    // Define the basket body.
    b2BodyDef basketBodyDef;
    basketBodyDef.type = b2_staticBody;
    CGPoint pos = ccp(tin.position.x / PTM_RATIO, (tin.position.y - 10) / PTM_RATIO);
    basketBodyDef.position.Set(pos.x, pos.y); // bottom-left corner
    
    b2Body *basketBody = world->CreateBody(&basketBodyDef);
    
    b2PolygonShape leftWall;
    leftWall.SetAsBox(0.05, 2.4, b2Vec2(-2, 0), CC_DEGREES_TO_RADIANS(0));
    //        basketShape.Set(b2Vec2(-2, 2), b2Vec2(-1, -2));
    b2FixtureDef leftWallFixtureDef;
    leftWallFixtureDef.shape = &leftWall;
    leftWallFixtureDef.density = 0;
    leftWallFixtureDef.filter.categoryBits = BASKET_WALLS;
    leftWallFixtureDef.filter.maskBits = BALL;
    b2Fixture *left = basketBody->CreateFixture(&leftWallFixtureDef);
    
    b2BodyDef basketBottomBodyDef;
    basketBottomBodyDef.type = b2_staticBody;
    basketBottomBodyDef.position.Set(pos.x, pos.y - 1.9); // bottom-left corner
    
    vert[0].Set(1.9, 2.6);
    vert[1].Set(1.9, 3.1);
    vert[2].Set(-1.9, 3.1);
    vert[3].Set(-1.9, 2.6);
    polygonShape.Set(vert, 4);
    b2Body *basketBottomBody = world->CreateBody(&basketBottomBodyDef);
    
    b2FixtureDef bottomWallFixtureDef;
    bottomWallFixtureDef.shape = &polygonShape;
    //        bottomWallFixtureDef.shape = &bottomWall;
    bottomWallFixtureDef.density = 0;
    bottomWallFixtureDef.filter.categoryBits = BASKET_WALLS;
    bottomWallFixtureDef.filter.maskBits = BALL;
    b2Fixture *bottom = basketBottomBody->CreateFixture(&bottomWallFixtureDef);
    bottom->SetUserData(tin);
    // Set the bottom fixture pointer of tin
    tin.bottomFixture = bottom;
    
    b2PolygonShape rightWall;
    rightWall.SetAsBox(0.05, 2.4, b2Vec2(2, 0), CC_DEGREES_TO_RADIANS(0));
    b2FixtureDef rightWallFixtureDef;
    rightWallFixtureDef.shape = &rightWall;
    rightWallFixtureDef.density = 0;
    rightWallFixtureDef.filter.categoryBits = BASKET_WALLS;
    rightWallFixtureDef.filter.maskBits = BALL;
    b2Fixture *right = basketBody->CreateFixture(&rightWallFixtureDef);
    
    contactListener->platforms.push_back(Fixture(left, 2.4));
    contactListener->platforms.push_back(Fixture(bottom, 0.5));
    contactListener->platforms.push_back(Fixture(right, 2.4));
}

- (CGImageRef)maskedImageFromImage:(NSString *)imageName andMask:(NSString *)maskName
{
    NSString *ipadhd = [[CCFileUtils sharedFileUtils] iPadRetinaDisplaySuffix];
    NSString *ipad = [[CCFileUtils sharedFileUtils] iPadSuffix];
    if (CC_CONTENT_SCALE_FACTOR() > 1.0)
    {
        imageName = [[imageName stringByDeletingPathExtension] stringByAppendingFormat:@"%@.%@", ipadhd, imageName.pathExtension];
        maskName = [[maskName stringByDeletingPathExtension] stringByAppendingFormat:@"%@.%@", ipadhd, maskName.pathExtension];
    }
    else
    {
        imageName = [[imageName stringByDeletingPathExtension] stringByAppendingFormat:@"%@.%@", ipad, imageName.pathExtension];
        maskName = [[maskName stringByDeletingPathExtension] stringByAppendingFormat:@"%@.%@", ipad, maskName.pathExtension];
    }
    
    UIImage *maskImage = [UIImage imageNamed:maskName];
    UIImage *image = [UIImage imageNamed:imageName];
    
    CGImageRef maskRef = maskImage.CGImage;
    
	CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
	CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    return masked;
}

- (void)createTins
{
    NSArray *tinData = [[gameData objectForKey:@"TinData"] retain];
    
    NSDictionary *tinDict = nil;
    NSString *tinFileName = nil;
    NSString *tinName = nil;
    NSArray *allowedType = nil;
    NSString *tinLogoFileName = nil;
    NSString *tinDescription = nil;
    
    // First Tin
    tinDict = [tinData objectAtIndex:0];
    tinFileName = [[tinDict objectForKey:kTinName] stringByAppendingPathExtension:@"png"];
    tinName = [tinDict objectForKey:kTinName];
    allowedType = [tinDict objectForKey:kAllowedType];
    tinLogoFileName = [tinName stringByReplacingOccurrencesOfString:@"Tin" withString:@"Logo.png"];
    tinDescription = [tinDict objectForKey:kTinDescription];
    CCSprite *frontTin0 = [CCSprite spriteWithCGImage:[self maskedImageFromImage:tinFileName andMask:@"TinMask.png"] key:tinName];
    CCSprite *tin0Logo = [CCSprite spriteWithFile:tinLogoFileName];
    
    Tin *tin0 = [[[Tin alloc] initWithFile:tinFileName name:tinName description:tinDescription allowedType:allowedType frontSprite:frontTin0 andTinLogo:tin0Logo] autorelease];
    tin0.position = ccp(screenSize.width/1.5, screenSize.height/2.5);
    [self createTinBodyOfTin:tin0];

    // Second Tin
    tinDict = [tinData objectAtIndex:1];
    tinFileName = [[tinDict objectForKey:kTinName] stringByAppendingPathExtension:@"png"];
    tinName = [tinDict objectForKey:kTinName];
    allowedType = [tinDict objectForKey:kAllowedType];
    tinLogoFileName = [tinName stringByReplacingOccurrencesOfString:@"Tin" withString:@"Logo.png"];
    tinDescription = [tinDict objectForKey:kTinDescription];
    CCSprite *frontTin1 = [CCSprite spriteWithCGImage:[self maskedImageFromImage:tinFileName andMask:@"TinMask.png"] key:tinName];
    CCSprite *tin1Logo = [CCSprite spriteWithFile:tinLogoFileName];
    
    Tin *tin1 = [[[Tin alloc] initWithFile:tinFileName name:tinName description:tinDescription allowedType:allowedType frontSprite:frontTin1 andTinLogo:tin1Logo] autorelease];
    tin1.position = ccp(screenSize.width/2.035, screenSize.height/2.5);
    [self createTinBodyOfTin:tin1];
    
    // Third Tin
    tinDict = [tinData objectAtIndex:2];
    tinFileName = [[tinDict objectForKey:kTinName] stringByAppendingPathExtension:@"png"];
    tinName = [tinDict objectForKey:kTinName];
    allowedType = [tinDict objectForKey:kAllowedType];
    tinLogoFileName = [tinName stringByReplacingOccurrencesOfString:@"Tin" withString:@"Logo.png"];
    tinDescription = [tinDict objectForKey:kTinDescription];
    CCSprite *frontTin2 = [CCSprite spriteWithCGImage:[self maskedImageFromImage:tinFileName andMask:@"TinMask.png"] key:tinName];
    CCSprite *tin2Logo = [CCSprite spriteWithFile:tinLogoFileName];
    
    Tin *tin2 = [[[Tin alloc] initWithFile:tinFileName name:tinName description:tinDescription allowedType:allowedType frontSprite:frontTin2 andTinLogo:tin2Logo] autorelease];
    tin2.position = ccp(screenSize.width/3.1, screenSize.height/2.5);
    [self createTinBodyOfTin:tin2];
    
    tins = [[NSArray arrayWithObjects:tin0, tin1, tin2, nil] retain];
    
    [tinData release];
}

- (void)drawTins
{
    for (Tin *tin in tins)
    {
        CCSprite *frontTin = tin.frontSprite;
        if (frontTin)
        {
            frontTin.position = tin.position;
            [self addChild:frontTin z:kFrontTin];
        }
        CCSprite *tinLogo = tin.tinLogo;
        if (tinLogo)
        {
            tinLogo.position = ccp(tin.position.x, tin.position.y + 200);
            [self addChild:tinLogo z:kTins];
        }
        
        [self addChild:tin z:kTins];
    }
}

- (void)createMovingHand
{
    // Create Hand Sprite
    CCSprite *handSprite = [CCSprite spriteWithFile:@"Hand.png"];
    handSprite.position = ccp(screenSize.width/2, handSprite.contentSize.height / 2 - 20);
    handSprite.isTouchEnabled = true;
    
    [self addChild:handSprite z:kGameElements tag:kMovingHand];
}

- (void)createHUD
{
    // Create Next Bar
    CCSprite *nextBar = [CCSprite spriteWithFile:@"NextBar.png"];
    nextBar.position = ccp(screenSize.width/1.1, screenSize.height/1.7);
    [hudLayer addChild:nextBar z:kHudLayer tag:kNextBar];
    
    // Create Menu
    CCMenuItem *leaderBoard = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"LeaderboardInnerButton.png"] selectedSprite:[CCSprite spriteWithFile:@"LeaderboardInnerButton.png"]
                                                               block:^(id sender) {
                                                           Leaderboard *leaderboardLayer = [[[Leaderboard alloc] init] autorelease];
                                                           ModalPopup *popup = [[[ModalPopup alloc] initWithNode:leaderboardLayer] autorelease];
                                                           leaderboardLayer.popupDelegate = popup;
                                                           popup.modalLayerDelegate = leaderboardLayer;
                                                           popup.pauseLayerDelegate = self;
                                                           [self addChild:popup z:INT_MAX];
                                                       }];
    CCMenuItem *rules = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"RulesInnerButton.png"] selectedSprite:[CCSprite spriteWithFile:@"RulesInnerButton.png"]
                                                 block:^(id sender) {
                                                     RulesLayer *rulesLayer = [[[RulesLayer alloc] init] autorelease];
                                                     ModalPopup *popup = [[[ModalPopup alloc] initWithNode:rulesLayer] autorelease];
                                                     rulesLayer.popupDelegate = popup;
                                                     popup.modalLayerDelegate = rulesLayer;
                                                     popup.pauseLayerDelegate = self;
                                                     [self addChild:popup z:INT_MAX];
                                                 }];
    CCMenuItem *legal = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"LegalInnerButton.png"] selectedSprite:[CCSprite spriteWithFile:@"LegalInnerButton.png"]
                                                 block:^(id sender) {
                                                     LegalLayer *legalLayer = [[[LegalLayer alloc] init] autorelease];
                                                     ModalPopup *popup = [[[ModalPopup alloc] initWithNode:legalLayer] autorelease];
                                                     legalLayer.popupDelegate = popup;
                                                     popup.modalLayerDelegate = legalLayer;
                                                     popup.pauseLayerDelegate = self;
                                                     [self addChild:popup z:INT_MAX];
                                                 }];
    CCMenu *menu = [CCMenu menuWithItems:leaderBoard, rules, legal, nil];
    menu.position = ccp(screenSize.width/1.23, screenSize.height/1.03);
    [menu alignItemsHorizontallyWithPadding:20];
    [hudLayer addChild:menu];
    
    // Create ScoreCard
    CCSprite *scoreCard = [CCSprite spriteWithFile:@"ScoreCard.png"];
    scoreCard.position = ccp(scoreCard.contentSize.width / 2, screenSize.height - scoreCard.contentSize.height / 2);
    [hudLayer addChild:scoreCard];
    
    // Create Score Label
    CCLabelBMFont *scoreLabel = [CCLabelBMFont labelWithString:@"0" fntFile:@"Score.fnt" width:kCCLabelAutomaticWidth alignment:kCCTextAlignmentCenter];
//    CCLabelTTF *scoreLabel = [CCLabelTTF labelWithString:@"0" dimensions:CGSizeMake(100, 25) hAlignment:kCCTextAlignmentCenter fontName:@"Helvetica-Bold" fontSize:26];
    scoreLabel.position = ccp(scoreCard.position.x, screenSize.height - 180);
    scoreLabel.tag = kScoreLabel;
//    scoreLabel.color = ccc3(235, 111, 63);
    [hudLayer addChild:scoreLabel];
    
    // Create Progress Timer
    ProgressTimer *progressTimer = [[[ProgressTimer alloc] initWithTime:time] autorelease];
    progressTimer.position = ccp(50, screenSize.height/2);
    [self addChild:progressTimer z:kHudLayer tag:kProgressBar];
    
    CCLabelTTF *timeLabel = [CCLabelTTF labelWithString:@"Time" fontName:@"Helvetica" fontSize:24];
    timeLabel.position = ccp(timeLabel.contentSize.width/1.15, screenSize.height/3.3);
    timeLabel.color = ccc3(0, 0, 0);
    [self addChild:timeLabel z:kHudLayer];
}

- (void)fillBugsInNextBar
{
    while (pestList.count < 3)
    {
        NSDictionary *pestDict = [pests objectAtIndex:arc4random() % pests.count];
        NSString *pestName = [pestDict objectForKey:kPestName];
        NSString *pestFileName = [[pestDict objectForKey:kPestName] stringByAppendingPathExtension:@"png"];
        NSString *pestType = [pestDict objectForKey:kPestType];
//        [pestList addObject:[CCSprite spriteWithFile:pest]];
        Pest *pest = [[[Pest alloc] initWithFile:pestFileName name:pestName andType:pestType] autorelease];
        [pestList addObject:pest];

        //[pestList addObject:pest];
    }
}

- (void)drawNextBar
{
    CCNode *nextBarNode = [hudLayer getChildByTag:kNextBar];
    //[nextBarNode removeAllChildrenWithCleanup:YES];
    for (int i = 0; i < pestList.count; i++)
    {
        CCSprite *pestSprite = [pestList objectAtIndex:i];
        if (![[nextBarNode children] containsObject:[pestList objectAtIndex:i]])
        {
            //[CCSprite spriteWithFile:[pestList objectAtIndex:i]];
            pestSprite.position = ccp(nextBarNode.contentSize.width / 2, 30);
            pestSprite.scale = 0;
            [nextBarNode addChild:pestSprite];
            
            [pestSprite runAction:[CCScaleTo actionWithDuration:0.2 scale:1]];
        }

        [pestSprite runAction:[CCMoveTo actionWithDuration:0.2 position:ccp(nextBarNode.contentSize.width / 2, (pestList.count - i - 1) * 110 + 80)]];
    }
}

- (void)preloadSounds
{
    SimpleAudioEngine *audioEngine = [SimpleAudioEngine sharedEngine];
    
    [audioEngine preloadEffect:@"Chimes.mp3"];
    [audioEngine preloadEffect:@"Whistle.wav"];
    [audioEngine preloadEffect:@"Squish.wav"];
    [audioEngine preloadEffect:@"Grumble.wav"];
    [audioEngine preloadEffect:@"Win.wav"];
    [audioEngine preloadEffect:@"Brush.wav"];
    [audioEngine preloadEffect:@"Grass.wav"];
    [audioEngine preloadEffect:@"Slide.wav"];
    [audioEngine preloadEffect:@"Bird.wav"];
    [audioEngine preloadEffect:@"Scream.wav"];
}

- (void)playBirdSound:(ccTime)dt
{
    [SimpleAudioEngine sharedEngine].backgroundMusicVolume = 0.6;
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Bird.wav" loop:false];
}

#pragma mark - Game Logic

- (void)throwPestWithImpulse:(b2Vec2)impulse
{
    [selectedPest stopAllActions];
    
    // Get the current location of the selected pest
    CGPoint currentPos = selectedPest.position;
    // DO THE MAGIC
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(currentPos.x / PTM_RATIO,currentPos.y/PTM_RATIO);
    bodyDef.bullet = true;
//    bodyDef.angularDamping = 0.8;
    b2Body *pestBody = world->CreateBody(&bodyDef);
    selectedPest.body = pestBody;
    
    b2PolygonShape polygonShape;
    CGRect box = selectedPest.boundingBox;
    polygonShape.SetAsBox(box.size.width / PTM_RATIO / 2, box.size.height / 2 / PTM_RATIO);
    /*b2CircleShape circleShape;
    circleShape.m_radius = 32 / PTM_RATIO;*/
    
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &polygonShape;
    fixtureDef.density = 0.9;
//    fixtureDef.friction = 1;
    fixtureDef.filter.categoryBits = BALL;
    fixtureDef.filter.maskBits = BOUNDARY | BASKET_WALLS | FLOOR;
    pestBody->CreateFixture( &fixtureDef);
    pestBody->SetSleepingAllowed(true);
    // Push the body in the air
    
    float volume = pestBody->GetMass() / 0.9;
    float newDensity = 5.0 / volume;
    pestBody->GetFixtureList()->SetDensity(newDensity);
    pestBody->ResetMassData();
    
    pestBody->ApplyLinearImpulse(impulse, pestBody->GetWorldCenter());
    pestBody->SetUserData(selectedPest);
    selectedPest = nil;
    
    // Play Whistle Sound
    [[SimpleAudioEngine sharedEngine] playEffect:@"Whistle.wav"];
}


- (void)placeBugOnHand
{
    if (isPaused)
        return;
    
//    CCSprite *bugSprite = [pestList objectAtIndex:0];
    Pest *bugSprite = [pestList objectAtIndex:0];
    [bugSprite stopAllActions];
    selectedPest = bugSprite;
    
    [[hudLayer getChildByTag:kNextBar] removeChild:bugSprite cleanup:NO];
    bugSprite.position = [self getChildByTag:kMovingHand].position;
    bugSprite.opacity = 0;
    [bugSprite runAction:[CCFadeIn actionWithDuration:0.2]];
    [self addChild:bugSprite z:kGameElements tag:kPest];
    [pestList removeObjectAtIndex:0];
    
    // Fill the list and draw it again
    [self fillBugsInNextBar];
    [self drawNextBar];
    
    // Fall the pest if the game type is different
    if ([gameType isEqualToString:kDifficult])
    {
        [selectedPest runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:pestTimeout]
                                 two:[CCCallBlockN actionWithBlock:^(CCNode *node) {
            Pest *pestSprite = (Pest *)node;
            if (pestSprite.position.x < screenSize.width / 2)
            {
                ccBezierConfig config;
                config.controlPoint_1 = pestSprite.position;
                config.controlPoint_2 = ccp(pestSprite.position.x + 50, 200);
                config.endPosition = ccp(pestSprite.position.x + 100, pestSprite.position.y);
                
                [pestSprite runAction:[CCSequence actionOne:[CCBezierTo actionWithDuration:0.5 bezier:config] two:[CCCallFunc actionWithTarget:pestSprite selector:@selector(destroyPest)]]];
            }
            else
            {
                ccBezierConfig config;
                config.controlPoint_1 = pestSprite.position;
                config.controlPoint_2 = ccp(pestSprite.position.x - 50, 200);
                config.endPosition = ccp(pestSprite.position.x - 100, pestSprite.position.y);
                
                [pestSprite runAction:[CCSequence actionOne:[CCBezierTo actionWithDuration:0.5 bezier:config] two:[CCCallFunc actionWithTarget:pestSprite selector:@selector(destroyPest)]]];
            }
            
            selectedPest = nil;
            [self placeBugOnHand];
        }]]];
    }
}

- (void)splashTin:(Tin *)tin
{
    CCSprite *splashSprite = [CCSprite spriteWithFile:@"TinSplash.png"];
    splashSprite.position = ccp(tin.position.x, tin.position.y + 95);
    
    [splashSprite runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:1.5] two:[CCCallFunc actionWithTarget:splashSprite selector:@selector(removeFromParentAndCleanup:)]]];
    
    [self addChild:splashSprite z:kTins];
    
    CCSprite *frontSplashSprite = [CCSprite spriteWithFile:@"TinFrontSplash.png"];
    frontSplashSprite.position = ccp(tin.position.x - 28, tin.position.y + 20);
    
    [frontSplashSprite runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:1.5] two:[CCCallFunc actionWithTarget:splashSprite selector:@selector(removeFromParentAndCleanup:)]]];
    
    [self addChild:frontSplashSprite z:kFrontTin];
}

- (void)updateScore
{
    // Update Score label
    CCLabelTTF *scoreLabel = (CCLabelTTF *)[hudLayer getChildByTag:kScoreLabel];
    [scoreLabel setString:[NSString stringWithFormat:@"%i", score]];
}

- (void)drawPlusPoint:(CCSprite *)tin
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"Chimes.mp3"];
    score += 50;
    
    Tin *tempTin = (Tin *)tin;
    tempTin.score += 50;
    tempTin.multiplier++;
    
    CCSprite *outerSpark = [CCSprite spriteWithFile:@"PositiveScoreGlow.png"];
    outerSpark.position = ccp(tin.position.x + 70, tin.position.y + 120);
    outerSpark.scale = 0.8;
    [self addChild:outerSpark z:kHUD];
    
    [outerSpark runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCMoveBy actionWithDuration:0.5 position:ccp(0, 200)], [CCFadeOut actionWithDuration:0.2], [CCCallFunc actionWithTarget:outerSpark selector:@selector(removeFromParentAndCleanup:)], nil]];
    
    CCSprite *spark = [CCSprite spriteWithFile:@"PositiveScoreGlow.png"];
    spark.opacity = 0;
    
    [outerSpark addChild:spark];
    
    CCAction *sparking = [CCRepeatForever actionWithAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCRotateBy actionWithDuration:0.4 angle:360], [CCFadeOut actionWithDuration:0.2], nil]];
    [spark runAction: sparking];
    
    CCSprite *glow = [CCSprite spriteWithFile:@"PositiveGlow.png"];
    glow.position = ccp(tin.position.x, tin.position.y + 80);
    glow.opacity = 0;
    
    [self addChild:glow];
    
    [glow runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCFadeOut actionWithDuration:0.2], [CCCallFunc actionWithTarget:glow selector:@selector(removeFromParentAndCleanup:)], nil]];
    
//    CCSprite *positivePoint = [CCSprite spriteWithFile:@"PlusPoint.png"];
    CCLabelBMFont *positivePoint = [CCLabelBMFont labelWithString:@"50" fntFile:@"PositivePoint.fnt"];
    positivePoint.position = ccp(tin.position.x + 60, tin.position.y + 120);
    positivePoint.scale = 0.75;
    
    [self addChild:positivePoint z:kHUD];
    
    [positivePoint runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCMoveBy actionWithDuration:0.5 position:ccp(0, 200)], [CCFadeOut actionWithDuration:0.2], [CCCallFunc actionWithTarget:positivePoint selector:@selector(removeFromParentAndCleanup:)], nil]];
}

- (void)drawMinusPoint:(CCSprite *)tin
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"NegativePoint.mp3"];
    score -= 50;
    
    Tin *tempTin = (Tin *)tin;
    tempTin.score -= 50;
    tempTin.multiplier++;
    
    CCSprite *outerSpark = [CCSprite spriteWithFile:@"NegativeScoreGlow.png"];
    outerSpark.position = ccp(tin.position.x - 80, tin.position.y + 120);
    [self addChild:outerSpark z:kHUD];
    
    [outerSpark runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCMoveBy actionWithDuration:0.5 position:ccp(0, 200)], [CCFadeOut actionWithDuration:0.2], [CCCallFunc actionWithTarget:outerSpark selector:@selector(removeFromParentAndCleanup:)], nil]];
    
    CCSprite *spark = [CCSprite spriteWithFile:@"NegativeScoreGlow.png"];
    spark.opacity = 0;
    
    [outerSpark addChild:spark];
    
    CCAction *sparking = [CCRepeatForever actionWithAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCRotateBy actionWithDuration:0.4 angle:360], [CCFadeOut actionWithDuration:0.2], nil]];
    [spark runAction: sparking];
    
    CCSprite *glow = [CCSprite spriteWithFile:@"NegativeGlow.png"];
    glow.position = ccp(tin.position.x, tin.position.y + 40);
    glow.opacity = 0;
    
    [self addChild:glow];
    
    [glow runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCFadeOut actionWithDuration:0.2], [CCCallFunc actionWithTarget:glow selector:@selector(removeFromParentAndCleanup:)], nil]];
    
    //CCSprite *negativePoint = [CCSprite spriteWithFile:@"MinusPoint.png"];
    CCLabelBMFont *negativePoint = [CCLabelBMFont labelWithString:@"-50" fntFile:@"NegativePoint.fnt"];
    negativePoint.position = ccp(tin.position.x - 90, tin.position.y + 120);
    negativePoint.scale = 0.75;
    
    [self addChild:negativePoint z:kHUD];
    
    [negativePoint runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.3], [CCMoveBy actionWithDuration:0.5 position:ccp(0, 200)], [CCFadeOut actionWithDuration:0.2], [CCCallFunc actionWithTarget:negativePoint selector:@selector(removeFromParentAndCleanup:)], nil]];
}

- (void)drawMissPoint:(CGPoint)pos
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"NegativePoint.mp3"];
    score -= 10;
    
    CCSprite *outerSpark = [CCSprite spriteWithFile:@"NegativeScoreGlow.png"];
    outerSpark.position = ccp(pos.x + 40, pos.y + 50);
    [self addChild:outerSpark z:kHUD];
    
    [outerSpark runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCMoveBy actionWithDuration:0.5 position:ccp(0, 200)], [CCFadeOut actionWithDuration:0.2], [CCCallFunc actionWithTarget:outerSpark selector:@selector(removeFromParentAndCleanup:)], nil]];
    
    CCSprite *spark = [CCSprite spriteWithFile:@"NegativeScoreGlow.png"];
    spark.opacity = 0;
    
    [outerSpark addChild:spark];
    
    CCAction *sparking = [CCRepeatForever actionWithAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.2], [CCRotateBy actionWithDuration:0.4 angle:360], [CCFadeOut actionWithDuration:0.2], nil]];
    [spark runAction: sparking];
    
//    CCLabelTTF *negativePoint = [CCLabelTTF labelWithString:@"-10" fontName:@"Helvetica-Bold" fontSize:32];
    CCLabelBMFont *negativePoint = [CCLabelBMFont labelWithString:@"-10" fntFile:@"NegativePoint.fnt"];
    negativePoint.position = ccp(pos.x + 30, pos.y + 50);
    negativePoint.scale = 0.75;
    
    [self addChild:negativePoint z:kHUD];
    
    [negativePoint runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.3], [CCMoveBy actionWithDuration:0.5 position:ccp(0, 200)], [CCFadeOut actionWithDuration:0.2], [CCCallFunc actionWithTarget:negativePoint selector:@selector(removeFromParentAndCleanup:)], nil]];
}

- (void)timeUp
{
    GameManager *manager = [GameManager gameManager];
    SummaryLayer *summary = [[[SummaryLayer alloc] initWithStageNum:manager.currentLevel tins:tins andScore:score] autorelease];
    ModalPopup *modalPopup = [[[ModalPopup alloc] initWithNode:summary] autorelease];
    modalPopup.modalLayerDelegate = summary;
    modalPopup.pauseLayerDelegate = self;
    [self addChild:modalPopup z:INT_MAX];
    
    isTimeOver = YES;
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"Win.wav"];
}

- (void)randomizeTins
{
    GameManager *gameManager = [GameManager gameManager];
    if (gameManager.currentScenario == 0)
    {
        int random = arc4random() % tins.count;
        for (int i = 0; i < tins.count; i++)
        {
            Tin *tin1 = [tins objectAtIndex:i];
            Tin *tin2 = [tins objectAtIndex:random];
            
            CGPoint tempPosition = tin2.position;
            
            [tin2 setTinPosition:tin1.position];
            [tin1 setTinPosition:tempPosition];
            
            // Replace the body userdata
            void *tempUserData = tin2.bottomFixture->GetUserData();
            b2Fixture *tempFixture = tin2.bottomFixture;
            tin2.bottomFixture->SetUserData(tin1.bottomFixture->GetUserData());
            tin1.bottomFixture->SetUserData(tempUserData);
            
            tin2.bottomFixture = tin1.bottomFixture;
            tin1.bottomFixture = tempFixture;
        }
    }
}

#pragma mark - Cocos2D and Box2D Methods
- (void)onEnter
{
    [super onEnter];
    
    // Create Transition
    CCSprite *rightGrass = [CCSprite spriteWithFile:@"RightGrass.png"];
    rightGrass.position = ccp(screenSize.width - rightGrass.contentSize.width/2, screenSize.height/2);
    CCSprite *leftGrass = [CCSprite spriteWithFile:@"LeftGrass.png"];
    leftGrass.position = ccp(leftGrass.contentSize.width/2, screenSize.height/2);
    CCSprite *grassBackground = [CCSprite spriteWithFile:@"GrassTransitionBackground.png"];
    grassBackground.position = ccp(screenSize.width/2, screenSize.height/2);
    
    [self addChild:grassBackground z:kTransition tag:kGrassBackground];
    [self addChild:rightGrass z:kTransition tag:kRightGrass];
    [self addChild:leftGrass z:kTransition tag:kLeftGrass];
    
    self.isTouchEnabled = false;
    
    // Add Level Page
    int currentLevel = [[GameManager gameManager] currentLevel];
    NSString *levelImageFileName = [NSString stringWithFormat:@"Level%i.png", currentLevel + 1];
    CCSprite *levelImage = [CCSprite spriteWithFile:levelImageFileName];
    levelImage.position = ccp(screenSize.width/2, screenSize.height/2);
    [self addChild:levelImage z:kTransition + 1];
    
    [levelImage runAction:[CCSequence actions:[CCDelayTime actionWithDuration:1.8], [CCFadeOut actionWithDuration:0.2], [CCCallFuncN actionWithTarget:levelImage selector:@selector(removeFromParentAndCleanup:)], nil]];
}

- (void)onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    [self runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:2] two:[CCCallFunc actionWithTarget:self selector:@selector(grassTransition)]]];
}

- (void)grassTransition
{
    // Start Transition
    CCSprite *grassBackground = (CCSprite *)[self getChildByTag:kGrassBackground];
    CCSprite *rightGrass = (CCSprite *)[self getChildByTag:kRightGrass];
    CCSprite *leftGrass = (CCSprite *)[self getChildByTag:kLeftGrass];
    
    float transitionSpeed = 1;
    [grassBackground runAction:[CCSequence actions:[CCFadeOut actionWithDuration:transitionSpeed], [CCCallFuncN actionWithTarget:grassBackground selector:@selector(removeFromParentAndCleanup:)], [CCCallBlock actionWithBlock:^{
        self.isTouchEnabled = true;
        [(ProgressTimer  *)[self getChildByTag:kProgressBar] startTimer];
        
        [self createMovingHand];
        [self placeBugOnHand];
        
        [self resume];
        
        [[SimpleAudioEngine sharedEngine] stopEffect:grassSound];
        [[SimpleAudioEngine sharedEngine] stopEffect:brushSound];
        
        // Schedule Sound 1:25 + 8 seconds
        [self playBirdSound:0];
        [self schedule:@selector(playBirdSound:) interval:85+8];
        
    }], nil]];
    [rightGrass runAction:[CCSequence actionOne:[CCMoveTo actionWithDuration:transitionSpeed position:ccp(screenSize.width + rightGrass.contentSize.width/2, screenSize.height/2)] two:[CCCallFuncN actionWithTarget:rightGrass selector:@selector(removeFromParentAndCleanup:)]]];
    [leftGrass runAction:[CCSequence actionOne:[CCMoveTo actionWithDuration:transitionSpeed position:ccp(-leftGrass.contentSize.width/2, screenSize.height/2)] two:[CCCallFuncN actionWithTarget:leftGrass selector:@selector(removeFromParentAndCleanup:)]]];
    
    grassSound = [[SimpleAudioEngine sharedEngine] playEffect:@"Grass.wav"];
    brushSound = [[SimpleAudioEngine sharedEngine] playEffect:@"Brush.wav"];
    
    [self pause];
}

- (void)onExitTransitionDidStart
{
    [[SimpleAudioEngine sharedEngine] stopEffect:grassSound];
    [[SimpleAudioEngine sharedEngine] stopEffect:brushSound];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    
    [SimpleAudioEngine sharedEngine].backgroundMusicVolume = 1.0;
}

- (void)dealloc
{
    // Body Loop
    std::vector<b2Body *>toDestroy;
    for(b2Body *b = world->GetBodyList(); b; b=b->GetNext())
    {
        toDestroy.push_back(b);
    }
    
    for (std::vector<b2Body *>::iterator itr = toDestroy.begin(); itr != toDestroy.end(); itr++)
    {
        b2Body *b = *itr;
        world->DestroyBody(b);
    }
    
    [gameData release];
    [gameType release];
    [tins release];
    [pestList makeObjectsPerformSelector:@selector(removeFromParentAndCleanup:)];
    [pestList removeAllObjects];
    [pestList release];
    [pests release];
    [hudLayer release];
    
    [self unscheduleAllSelectors];
    [self removeAllChildrenWithCleanup:YES];
    
    [super dealloc];
}

- (void)update:(ccTime)dt
{
    [self updateScore];
    if (selectedPest)
    {
        selectedPest.position = [self getChildByTag:kMovingHand].position;
    }
    
    //It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
	
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
    // Body Loop
    for(b2Body *b = world->GetBodyList(); b; b=b->GetNext())
    {
        if (b->GetLinearVelocity().y < 0)
        {
            b->ApplyForceToCenter(b2Vec2(0, -100));
        }
        
        if (b->GetUserData() != NULL)
        {
            CCSprite *sprite = (CCSprite *)b->GetUserData();
            if (sprite == NULL || [sprite isKindOfClass:[Tin class]])
                break;
            
            sprite.position = ccp(b->GetPosition().x * PTM_RATIO,
                                  b->GetPosition().y * PTM_RATIO);
            sprite.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
        }
    }
    
    std::vector<b2Body *>toDestroy;
    for(b2Body *b = world->GetBodyList(); b; b=b->GetNext())
    {
        if (!b->IsAwake() && b->GetUserData() != NULL)
        {
            toDestroy.push_back(b);
        }
    }
    
    for (std::vector<b2Body *>::iterator itr = toDestroy.begin(); itr != toDestroy.end(); itr++)
    {
        b2Body *b = *itr;
        CCSprite *sprite = (CCSprite *)b->GetUserData();
        if (sprite == NULL || ![sprite isKindOfClass:[Pest class]])
            continue;
        
        Pest *pest = (Pest *)sprite;
        [pest destroyPest];
        world->DestroyBody(b);
    }
    toDestroy.clear();
}

#pragma mark - Touch Handlers
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[self placeBugOnHand];
    UITouch* myTouch = [touches anyObject];
	CGPoint location = [myTouch locationInView: [myTouch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	location = [self convertToNodeSpace:location];
    
    firstTouch = location;
    
    // Check if it is on the hand
    CCNode *handSprite = [self getChildByTag:kMovingHand];
    if (CGRectContainsPoint(handSprite.boundingBox, location))
    {
        CGPoint newPosition = ccp(location.x, handSprite.position.y);
        handSprite.position = newPosition;
        
        isMoving = true;
    }
}

- (void)ccTouchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
	UITouch* myTouch = [touches anyObject];
	CGPoint location = [myTouch locationInView: [myTouch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	location = [self convertToNodeSpace:location];
    
    lastTouch = location;
    
    CGPoint direction = ccpSub(lastTouch, firstTouch);
    
    CCNode *handSprite = [self getChildByTag:kMovingHand];
    if (direction.y > kMaxYPower)
    {
        isMoving = false;
        firstTime = myTouch.timestamp;
    }
    else if (abs(direction.x) > kMaxYPower)
        isMoving = true;
    
    if (isMoving)
    {
        CGPoint newPosition = ccp(location.x, handSprite.position.y);
        handSprite.position = newPosition;
    }
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* myTouch = [touches anyObject];
	CGPoint location = [myTouch locationInView: [myTouch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	location = [self convertToNodeSpace:location];
    
    lastTouch = location;
    lastTime = myTouch.timestamp;
    
    CGPoint direction = ccpSub(lastTouch, firstTouch);
    CCNode *handSprite = [self getChildByTag:kMovingHand];
    
    CGFloat distance = ccpDistance(lastTouch, firstTouch);
    
    if (!isMoving && direction.y > kMaxYPower)
    {
        float touchTime = lastTime - firstTime;
        float velocityY = direction.y / touchTime;
        
        [self throwPestWithImpulse:b2Vec2(0, distance * 8.5 / PTM_RATIO)];
        [self placeBugOnHand];
    }
    else
    {
        CGPoint newPosition = ccp(location.x, handSprite.position.y);
        handSprite.position = newPosition;
    }
    
	[self ccTouchesCancelled:touches withEvent:event];
}

- (void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    isMoving = false;
}

#pragma mark - PauseLayerDelegate Methods
- (void)pause
{
    isPaused = true;
 
    CCSprite *cloud1, *cloud2;
    cloud1 = (CCSprite *)[self getChildByTag:kCloud1];
    cloud2 = (CCSprite *)[self getChildByTag:kCloud2];
    
    [self unscheduleUpdate];
    
    [cloud1 pauseSchedulerAndActions];
    [cloud2 pauseSchedulerAndActions];
    
    if (selectedPest)
        [selectedPest pauseSchedulerAndActions];
    
    ProgressTimer *progressTimer = (ProgressTimer *)[self getChildByTag:kProgressBar];
    [progressTimer pauseSchedulerAndActions];
}

- (void)resume
{
    isPaused = false;
    
    if (!isTimeOver)
    {
        CCSprite *cloud1, *cloud2;
        cloud1 = (CCSprite *)[self getChildByTag:kCloud1];
        cloud2 = (CCSprite *)[self getChildByTag:kCloud2];
        
        [self scheduleUpdate];
        
        if (selectedPest)
            [selectedPest resumeSchedulerAndActions];
        else
            [self placeBugOnHand];
        
        [cloud1 resumeSchedulerAndActions];
        [cloud2 resumeSchedulerAndActions];
        
        ProgressTimer *progressTimer = (ProgressTimer *)[self getChildByTag:kProgressBar];
        [progressTimer resumeSchedulerAndActions];
    }
    else if (isTimeOver && [[GameManager gameManager] currentLevel] < 0)
    {
        [[GameManager gameManager] sendScore];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[IntroLayer scene] withColor:ccWHITE]];
    }
}

- (void)replayLevel
{
    // Change Level
//    GameManager *manager = [GameManager gameManager];
    //manager.score += score;
    //[manager moveToNextLevel];
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameScene scene] withColor:ccWHITE]];
}

- (void)nextLevel
{
    NSLog(@"Next Level");
    // Change Level
    GameManager *manager = [GameManager gameManager];
    manager.score += score;
    [manager moveToNextLevel];
    
    if (manager.currentLevel >= 0)
    {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameScene scene] withColor:ccWHITE]];
    }
    else
    {
        LegalLayer *legalLayer = [[[LegalLayer alloc] init] autorelease];
        legalLayer.blockClose = true;
        ModalPopup *popup = [[[ModalPopup alloc] initWithNode:legalLayer] autorelease];
        legalLayer.popupDelegate = popup;
        popup.modalLayerDelegate = legalLayer;
        popup.pauseLayerDelegate = self;
        [self addChild:popup z:INT_MAX];
        // Go back to intro screen
//        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[IntroLayer scene] withColor:ccWHITE]];
    }
}

@end
