//
//  Pest.m
//  Box2D
//
//  Created by Azeem on 28/11/2012.
//
//

#import "Pest.h"
#import "GameScene.h"

@implementation Pest
@synthesize checkCollision, name, isDead, body, shallAddPoint, type, isOut;

- (id)initWithFile:(NSString *)filename name:(NSString *)aName andType:(NSString *)aType
{
    self = [super initWithFile:filename];
    if (self) {
        checkCollision = false;
        isDead = false;
        _startChecking = false;
        _endPoint = CGPointZero;
        shallAddPoint = YES;
        isOut = false;
        
        deadFileName = [[@"Dead" stringByAppendingString:filename] retain];
        name = [aName retain];
        type = [aType retain];
    }
    return self;
}

- (void)dealloc
{
    body = NULL;
    [deadFileName release];
    [name release];
    [type release];
    
    [super dealloc];
}

- (void)die
{
    NSLog (@"Dead");
    isDead = true;
    _startChecking = false;
    //checkCollision = false;
    
    if ([type isEqualToString:@"Disease"])
        [[SimpleAudioEngine sharedEngine] playEffect:@"Squish.wav"];
    else if ([type isEqualToString:@"Insect"])
        [[SimpleAudioEngine sharedEngine] playEffect:@"Scream.wav"];
    else if ([type isEqualToString:@"Weed"])
        [[SimpleAudioEngine sharedEngine] playEffect:@"Grumble.wav"];
    
    CCSprite *deadSprite = [CCSprite spriteWithFile:deadFileName];
    if (!deadSprite)
        return;
    [self setTexture:deadSprite.texture];
    self.textureRect = deadSprite.textureRect;
}

- (void)destroyPest
{
    // It means that the pest hasn't scored
    if (shallAddPoint)
    {
        GameScene *gameScene = [[[[CCDirector sharedDirector] runningScene] children] lastObject];
        [gameScene drawMissPoint:self.position];
        shallAddPoint = NO;
    }
    [self runAction:[CCSequence actions:[CCFadeOut actionWithDuration:0.5], [CCCallBlockN actionWithBlock:^(CCNode *node) {
        [node removeFromParentAndCleanup:YES];
    }], nil]];
}

- (void)setEndingPoint:(CGPoint)endPoint
{
    _startChecking = true;
    _endPoint = endPoint;
}

@end
