//
//  ModalPopup.m
//  Box2D
//
//  Created by Azeem on 16/11/2012.
//
//

#import "ModalPopup.h"

@implementation ModalPopup
@synthesize pauseLayerDelegate, modalLayerDelegate;

- (id) initWithNode:(CCNode *)aNode
{
    self = [super init];
    if (self)
    {
        backgroundSprite = [CCSprite spriteWithFile:@"PopupFadeBackground.png"];
        backgroundSprite.opacity = 0;
        [self addChild:backgroundSprite];
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CGPoint position = ccp(size.width / 2, size.height / 2);
        
        self.position = position;
        
        self.isTouchEnabled = YES;
        
        // Add Sprite
        innerNode = [aNode retain];
//        layer.opacity = 0;
        innerNode.scale = 0;
        [self addChild:innerNode];
    }
    return self;
}

- (void)setMenuStatus:(bool)status node:(CCNode *)node
{
    for (id result in node.children)
    {
        if (result == self)
            continue;
        
		if ([result isKindOfClass:[CCMenu class]])
        {
			for (id child in ((CCMenu *)result).children)
            {
				if ([child isKindOfClass:[CCMenuItem class]])
                {
					((CCMenuItem *)child).isEnabled = status;
				}
			}
		}
        else
            [self setMenuStatus:status node:result];
	}
}

- (void)onEnter
{
    [super onEnter];
    [pauseLayerDelegate pause];
    
    // Disable underlying menus
    [self setMenuStatus:false node:self.parent];
    
    float fadeDuration = 0.3;
    
    [backgroundSprite runAction:[CCFadeIn actionWithDuration:fadeDuration]];
    
    [innerNode runAction:[CCSpawn actions:/*[CCFadeIn actionWithDuration:fadeDuration], */[CCScaleTo actionWithDuration:fadeDuration scale:1.0], nil]];
}

- (void)registerWithTouchDispatcher
{
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return true;
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
//    CGPoint touchLocation = [self convertTouchToNodeSpace: touch];
    CCNode *dialogBox = innerNode;
    
    CGRect dialogBoxBounds = CGRectMake(self.position.x - dialogBox.boundingBox.size.width / 2, 0, dialogBox.boundingBox.size.width, dialogBox.boundingBox.size.height);
    
    if (!CGRectContainsPoint(dialogBoxBounds, [touch locationInView:[touch view]]))
    {
        if ([modalLayerDelegate shallClose])
        {
            [self removePopup];
        }
        return;
    }
}

- (void)removePopup
{
    [[[CCDirector sharedDirector] touchDispatcher] removeDelegate:self];
    
    float fadeDuration = 0.5;
    [backgroundSprite runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:fadeDuration] two:[CCCallFunc actionWithTarget:self selector:@selector(removeFromParentAndCleanup:)]]];
    
    CCSequence *spriteRemove = [CCSequence actions:[CCSpawn actions:/*[CCFadeOut actionWithDuration:fadeDuration],*/ [CCScaleTo actionWithDuration:fadeDuration scale:0.0], nil], [CCCallFuncN actionWithTarget:innerNode selector:@selector(removeFromParentAndCleanup:)], nil];
    
    [innerNode runAction:spriteRemove];
}

- (void)onExit
{
    [pauseLayerDelegate resume];
    
    [innerNode release];
    // Enable underlying menus
    [self setMenuStatus:true node:self.parent];
    [[[CCDirector sharedDirector] touchDispatcher] removeDelegate:self];

    [super onExit];
}

- (void)closePopup
{
    [self removePopup];
}

@end
