//
//  GameManager.h
//  Box2D
//
//  Created by Azeem on 02/12/2012.
//
//

#import <Foundation/Foundation.h>
#import "Player.h"

#define kLeaderboardGetURL @"http://slamit.ca/pestpong/api/UserScore.php?format=json&scenario=%i"
#define kDataGetURL @"http://slamit.ca/pestpong/api/apiaccess.php?format=json"
#define kLeaderboardPostURL @"http://slamit.ca/pestpong/api/post/users.php?userfullname=%@&email=%@&score=%i&registration_date=%@&scenario=%i"

#define kPasswordKey @"apiuserpass"
#define kLevelTimeKey @"timelimit"
#define kPestTimeoutKey @"release_bug"

#define kFullName @"fullname"
#define kUserScore @"user_score"
#define kCurrentDate @"current_date_time"
#define kEmail @"email"

#define kSimple @"Simple"
#define kDifficult @"Difficult"

#define kPestName @"Name"
#define kPestType @"Type"

#define kTinName @"Name"
#define kAllowedType @"AllowedPest"
#define kTinDescription @"Description"

@interface GameManager : NSObject <UIAlertViewDelegate>
{
    NSString *password;
}

@property (nonatomic, retain) Player *player;
@property (nonatomic) int score;
@property (nonatomic) int currentLevel;
@property (nonatomic) int currentScenario;
@property (nonatomic) float pestTimeOut;
@property (nonatomic) float levelTimeOut;

+ (id)gameManager;
- (NSDictionary *)getDataForScenario:(int)aScenario andLevel:(int)aLevel;
- (void)moveToNextLevel;
- (void)resetGame;
- (NSArray *)getLeaderBoardScores;
- (void)sendScore;
- (void)subscribeWithName:(NSString *)name andEmail:(NSString *)email;

@end
