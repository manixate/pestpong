//
//  ModalPopup.h
//  Box2D
//
//  Created by Azeem on 16/11/2012.
//
//

#import "cocos2d.h"

@protocol PauseLayerDelegate <NSObject>
- (void)pause;
- (void)resume;
@end

@protocol ModalLayerDelegate <NSObject>
- (BOOL)shallClose;
@end

@protocol PopupDelegate <NSObject>
- (void)closePopup;
@end

@interface ModalPopup : CCLayer <CCTargetedTouchDelegate, PopupDelegate>
{
    CCSprite *backgroundSprite;
    CCNode *innerNode;
}

@property (nonatomic, assign) id<PauseLayerDelegate> pauseLayerDelegate;
@property (nonatomic, assign) id<ModalLayerDelegate> modalLayerDelegate;

- (id) initWithNode:(CCNode *)aNode;

@end
