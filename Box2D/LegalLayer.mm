//
//  Legal.m
//  Box2D
//
//  Created by Azeem on 03/12/2012.
//
//

#import "LegalLayer.h"
#import "ModalPopup.h"
#import "GameManager.h"

#define kNameTextField  1
#define kEmailTextField 2
#define kCheckBoxSprite 3

@implementation LegalLayer

@synthesize nameTextView, emailTextView, popupDelegate, blockClose, name, email;

- (id)init
{
    self = [super init];
    if (self)
    {
        //CGSize screenSize = [[CCDirector sharedDirector] winSize];
        CCSprite *background = [CCSprite spriteWithFile:@"BasicPopup.png"];
        [self addChild:background];
        
        self.contentSize = background.contentSize;
        
        blockClose = false;
        
        GameManager *gameManager = [GameManager gameManager];
        Player *player = gameManager.player;
        
        name = player.name;
        email = player.email;
        isChecked = false;//player.isSubscribed;
        
        title = [CCLabelBMFont labelWithString:@"Terms & Conditions" fntFile:@"Title.fnt"];//[CCLabelTTF labelWithString:@"Terms & Conditions" fontName:@"Soup Of Justice" fontSize:48];
//        title.color = ccc3(235, 111, 63);
		title.position = ccp(0, 320);
		[self addChild:title];
        
        CCLabelTTF *nameLabel = [CCLabelTTF labelWithString:@"Name" fontName:@"Helvetica" fontSize:18];
        nameLabel.position = ccp(-180, 220);
        nameLabel.color = ccc3(219, 223, 100);
        [self addChild:nameLabel];
        CCSprite *nameLabelBar = [CCSprite spriteWithFile:@"TextBar.png"];
        nameLabelBar.position = ccp(32, 215);
        [self addChild:nameLabelBar];
        
        CCLabelTTF *emailLabel = [CCLabelTTF labelWithString:@"Email" fontName:@"Helvetica" fontSize:18];
        emailLabel.position = ccp(-182, 170);
        emailLabel.color = ccc3(219, 223, 100);
        [self addChild:emailLabel];
        CCSprite *emailLabelBar = [CCSprite spriteWithFile:@"TextBar.png"];
        emailLabelBar.position = ccp(32, 165);
        [self addChild:emailLabelBar];
        
        nameTextView = [[UITextField alloc] init];
        nameTextView.frame = CGRectMake(392, 155, 300, 34);
        nameTextView.backgroundColor = [UIColor clearColor];
        nameTextView.textColor = [UIColor whiteColor];
        nameTextView.font = [UIFont fontWithName:@"Helvetica" size:18];
        nameTextView.text = @"";
        nameTextView.tag = kNameTextField;
        nameTextView.delegate = self;
        
        [[[CCDirector sharedDirector] view] addSubview:nameTextView];
        
        emailTextView = [[UITextField alloc] init];
        emailTextView.frame = CGRectMake(392, 205, 300, 34);
        emailTextView.backgroundColor = [UIColor clearColor];
        emailTextView.textColor = [UIColor whiteColor];
        emailTextView.font = [UIFont fontWithName:@"Helvetica" size:18];
        emailTextView.text = @"";
        emailTextView.tag = kEmailTextField;
        emailTextView.delegate = self;
        
        [[[CCDirector sharedDirector] view] addSubview:emailTextView];
        
        uncheckedTex = [[CCSprite spriteWithFile:@"Unchecked.png"] texture];
        checkedTex = [[CCSprite spriteWithFile:@"Checked.png"] texture];
        
        CCSprite *checkBoxSprite = [CCSprite spriteWithTexture:(isChecked ? checkedTex : uncheckedTex)];
        checkBoxSprite.position = ccp(-190, 120);
        checkBoxSprite.tag = kCheckBoxSprite;
        [self addChild:checkBoxSprite];
        
        checkBox = [UIButton buttonWithType:UIButtonTypeCustom];
        checkBox.frame = CGRectMake(311, 252, 26, 24);
        [checkBox addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchDown];
        checkBox.backgroundColor = [UIColor clearColor];
        
        [[[CCDirector sharedDirector] view] addSubview:checkBox];
        
        CCLabelTTF *checkBoxText = [CCLabelTTF labelWithString:@"Opt in for a chance to win!" fontName:@"Helvetica" fontSize:18];
        checkBoxText.anchorPoint = ccp(0, 0.5);
        checkBoxText.position = ccp(-130, 120);
        checkBoxText.color = ccc3(219, 223, 100);
        [self addChild:checkBoxText];
        
        CCSprite *submitSprite = [CCSprite spriteWithFile:@"SubmitButton.png"];
        submitSprite.position = ccp(148, 120);
        [self addChild:submitSprite];
        
        submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        submitButton.selected = NO;
        submitButton.frame = CGRectMake(615, 244, submitSprite.contentSize.width, submitSprite.contentSize.height);
        [submitButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchDown];
        submitButton.backgroundColor = [UIColor clearColor];
        
        [[[CCDirector sharedDirector] view] addSubview:submitButton];
        
        CCLabelTTF *longDetailText = [CCLabelTTF labelWithString:@"By checking this box you are consenting to participating in the Hot Potatoes Grower Rewards Program and/or the Bayer CropScience Bayer Value Program. Full program Rules can be found at hot-potatoes.ca or BayerCropScience.ca. By checking this box, you agree to the use of your contact information by Bayer CropScience for the sole purpose of communicating with you about new research, products, field resources and promotions that may interest you. By participating you consent to the collection, use and disclosure by Bayer CropScience or its agents of personal information from you for the purposes of better understanding your needs and preferences in order to develop and offer information, products and/or services by Bayer CropScience or Bayer CropScience's authorized distributors/retailers.  Your information is guarded under our privacy policy and will not be shared or used for any other purpose than the aforementioned. \n\nIf you do not wish personal information to be used for developing and offering information, products and services, please specifically advise the Bayer CropScience privacy officer at Bayer CropScience Privacy Office, Suite 200, 160 Quarry Park Blvd. SE, Calgary, AB T2C 3G3." dimensions:CGSizeMake(background.contentSize.width - 145, 400) hAlignment:kCCTextAlignmentLeft lineBreakMode:kCCLineBreakModeWordWrap fontName:@"Helvetica" fontSize:14];
        longDetailText.anchorPoint = ccp(1, 1);
        longDetailText.position = ccp(195, 90);
        longDetailText.color = ccc3(219, 223, 100);
        [self addChild:longDetailText];
        
//        [self runAction:[CCSequence actionOne:[CCDelayTime actionWithDuration:0.3] two:[CCCallBlock actionWithBlock:^{
//            nameTextView.text = name;
//            emailTextView.text = email;
//        }]]];
    }
    return self;
}

- (BOOL)validateValues
{
    if ([name isEqualToString:@""] || [email isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incomplete Information" message:@"Please fill the required fields or you can un-check the checkbox and unsubscribe from the offer." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return false;
    }
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:email] == NO) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Email!" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return false;
    }
    
    return true;
}

- (void)onExitTransitionDidStart
{
    checkBox.enabled = false;
    submitButton.enabled = false;
}

- (void)onExit
{
    if ([nameTextView isFirstResponder])
        [nameTextView resignFirstResponder];
    if ([emailTextView isFirstResponder])
        [emailTextView resignFirstResponder];
    
    [nameTextView removeFromSuperview];
    [emailTextView removeFromSuperview];
    
    [nameTextView release];
    [emailTextView release];
    
    [name release];
    [email release];
    
    [checkBox removeFromSuperview];
    [submitButton removeFromSuperview];
    
    [super onExit];
}

#pragma mark - Modal Layer Delegate
- (BOOL)shallClose
{
    if ([nameTextView isFirstResponder])
        [nameTextView resignFirstResponder];
    if ([emailTextView isFirstResponder])
        [emailTextView resignFirstResponder];
    
    if (isChecked)
    {
        if ([self validateValues])
        {
            [[GameManager gameManager] subscribeWithName:name andEmail:email];
            [nameTextView removeFromSuperview];
            [emailTextView removeFromSuperview];
            return true;
        }
        else
            return false;
    }
    // It means that we haven't checked the box
    else if (blockClose)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Empty Fields!" message:@"Please enter valid information before closing it." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return false;
    }
    return true;
}

#pragma mark - Touch
- (void)buttonTapped:(UIButton *)sender
{
    if (sender == checkBox)
    {
        isChecked = !isChecked;
        
        CCSprite *checkBoxSprite = (CCSprite *)[self getChildByTag:kCheckBoxSprite];
        [checkBoxSprite setTexture:(isChecked ? checkedTex : uncheckedTex)];
    }
    else if (sender == submitButton)
    {
        if ([self shallClose])
            [popupDelegate closePopup];
    }
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == nameTextView)
        self.name = nameTextView.text;
    else if (textField == emailTextView)
        self.email = emailTextView.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    if (textField == nameTextView)
    {
        [emailTextView becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}

@end
